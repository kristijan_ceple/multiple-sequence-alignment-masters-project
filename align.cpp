//
// Created by kikyy_99 on 10. 01. 2022..
//

#include "ALS_GUI/als_gui.h"
#include "ALS_GUI/ui_als_gui.h"
#include "ALS_Algos_DataStructs/MyUtil.h"
#include "ALS_Algos_DataStructs/AlignmentAlgorithm.h"
#include "ALS_Algos_DataStructs/NeedlemanWunschAlgorithm.h"
#include "ALS_Algos_DataStructs/ProgressiveMSA_HMM_Alignment_Algorithm.h"
#include <QMessageBox>
#include <QTextStream>

using namespace std;

/**
 * Debug function
 */
//void ALS_GUI::align() {
//    this->trainingSequences = MyUtil::IOOps::readSequencesFile("../Sandbox/HIV1_REF_2010_genome_DNA.fasta");
//    this->inputSequences = MyUtil::IOOps::readSequencesFile("../Sandbox/toAlignPair1.fasta");
//
//    // TODO: Logic and work here
//    unique_ptr<NeedlemanWunschAlgorithm> alignAlgo = nullptr;
//    alignAlgo = make_unique<NeedlemanWunschAlgorithm>(this->trainingSequences, this->inputSequences);
//    alignAlgo->alignSequences();
//
//    // Print diagnostics here
//    cout << endl << "[INFO] Printing NW Matrices:" << endl;
//    cout << alignAlgo->matricesString() << endl;
//
//    for(const auto& seq : this->inputSequences) {
//        cout << string(seq.begin(), seq.end()) << endl;
//    }
//}

void ALS_GUI::align() {
    // Check whether all 3 files have been set
    if(this->trainingFile.fileName() == "" || this->inputFile.fileName() == "" || this->outputFile.fileName() == "") {
        QMessageBox::warning(this, "Warning", "All 3 files have to be specified!");
        return;
    }

    // TODO: Application code and binding here!
    QMessageBox::information(this, "Info", "Aligning...");

    // Check whether we're doing pairwise(NW) or MSA alignment
    QAbstractButton* checkedButton = this->ui->buttonGroup_AlgoSelect->checkedButton();
    if(checkedButton == nullptr) {
        QMessageBox::warning(this, "Warning", "Please specify the algorithm!");
        return;
    }

    QString selectedAlgo = checkedButton->text();

    // Open and read files into RAM, call the library functions as necessary!
    this->trainingSequences = MyUtil::IOOps::readSequencesFile(this->trainingFile.filesystemFileName());
    this->inputSequences = MyUtil::IOOps::readSequencesFile(this->inputFile.filesystemFileName());

    // TODO: Logic and work here
    unique_ptr<AlignmentAlgorithm> alignAlgo = nullptr;
    double emissionPseudocountTmp, transitionPseudocountTmp, columnRemovalThresholdTmp;
    if(selectedAlgo == MyUtil::GUI::NW_STRING) {
        // Needleman - Wunsch
        alignAlgo = make_unique<NeedlemanWunschAlgorithm>(this->trainingSequences, this->inputSequences);
    } else {
        // MSA
        // Load and check pseudocount values
        QString tmpStr;
        bool tmpOk;
        double res;

        // 1 -> Emission Pseudocount
        tmpStr = this->ui->lineEdit_emissionPseudocount->text();
        tmpOk = this->userInput2Double(tmpStr, &res, false);
        emissionPseudocountTmp = tmpOk ? res : MSA::EMISSION_PSEUDOCOUNT_DEFAULT_VALUE;

        // 2 -> Transition Pseudocount
        tmpStr = this->ui->lineEdit_transitionPseudocount->text();
        tmpOk = this->userInput2Double(tmpStr, &res, false);
        transitionPseudocountTmp = tmpOk ? res : MSA::TRANSITION_PSEUDOCOUNT_DEFAULT_VALUE;

        // 3 -> Column Removal Threshold
        tmpStr = this->ui->lineEdit_columnRemovalThreshold->text();
        tmpOk = this->userInput2Double(tmpStr, &res, true);
        columnRemovalThresholdTmp = tmpOk ? res : MSA::COLUMN_REMOVAL_THRESHOLD_DEFAULT_VALUE;

        alignAlgo = make_unique<ProgressiveMSA_HMM_Alignment_Algorithm>(
                this->trainingSequences, this->inputSequences,
                emissionPseudocountTmp, transitionPseudocountTmp, columnRemovalThresholdTmp
                );
    }

    alignAlgo->alignSequences();

    // Write to output file
    if(!this->outputFile.open(QIODevice::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, "Warning", "Cannot save file: " + this->outputFile.errorString());
        return;
    }

    QTextStream out(&this->outputFile);
    string res = MyUtil::Algorithm::sequencesToString(this->inputSequences);
    cout << "Final results:" << endl << res << endl;
    out << QString::fromStdString(res) << "\n";
    this->outputFile.close();
}