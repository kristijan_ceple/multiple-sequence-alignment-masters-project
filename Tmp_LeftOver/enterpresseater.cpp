#include "enterpresseater.h"
#include <QEvent>
#include <QKeyEvent>
#include <QMessageBox>
#include "als_gui.h"

EnterPressEater::EnterPressEater(QObject *parent)
    : QObject{parent}
{
    this->als_gui_ptr = reinterpret_cast<ALS_GUI*>(parent);
}

bool EnterPressEater::eventFilter(QObject *obj, QEvent *event) {
    if(event->type() == QEvent::KeyPress) {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        if(keyEvent->key() == Qt::Key_Return) {
            // Enter was pressed
            QMessageBox::information(this->als_gui_ptr, "Information", "Selected file for output!");
            this->als_gui_ptr->setOutputFile();
            return true;        // Dont forward the event to the text box
        } else {
            // Standard event processing
            return QObject::eventFilter(obj, event);
        }
    } else {
        // Standard Event processing
        return QObject::eventFilter(obj, event);
    }
}
