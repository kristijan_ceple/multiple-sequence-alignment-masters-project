//
// Created by kikyy_99 on 06.10.21.
//

#ifndef MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_SINGLETON_H
#define MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_SINGLETON_H

#include <memory>

/**
 * Modelling Singleton class as a singleton.
 * TODO: Multi-threading later on?
 */
class Singleton {
protected:
    Singleton() {};
    static inline std::shared_ptr<Singleton> singleton;
public:
    /**
     * Singletons shouldn't be cloneable. Copy constructor is therefore deleted!
     */
    Singleton(const Singleton& other) = delete;

    /**
     * Singletons shouldn't be assigneable
     */
    void operator=(const Singleton& other) = delete;

    static inline std::shared_ptr<Singleton> getInstance() {
        if(!Singleton::singleton) {
            Singleton::singleton.reset(new Singleton());
        }

        return Singleton::singleton;
    }
};


#endif //MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_SINGLETON_H
