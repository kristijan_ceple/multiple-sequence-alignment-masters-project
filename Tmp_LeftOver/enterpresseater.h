#ifndef ENTERPRESSEATER_H
#define ENTERPRESSEATER_H

#include <QObject>

class ALS_GUI;

class EnterPressEater : public QObject
{
    Q_OBJECT
public:
    explicit EnterPressEater(QObject* parent = nullptr);

signals:

protected:
    bool eventFilter(QObject *obj, QEvent *event) override;

private:
    ALS_GUI *als_gui_ptr = nullptr;
};

#endif // ENTERPRESSEATER_H
