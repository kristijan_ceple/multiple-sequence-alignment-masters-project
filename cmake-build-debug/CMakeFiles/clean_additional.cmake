# Additional clean files
cmake_minimum_required(VERSION 3.16)

if("${CONFIG}" STREQUAL "" OR "${CONFIG}" STREQUAL "Debug")
  file(REMOVE_RECURSE
  "ALS_autogen"
  "CMakeFiles/ALS_autogen.dir/AutogenUsed.txt"
  "CMakeFiles/ALS_autogen.dir/ParseCache.txt"
  )
endif()
