/********************************************************************************
** Form generated from reading UI file 'als_gui.ui'
**
** Created by: Qt User Interface Compiler version 6.2.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ALS_GUI_H
#define UI_ALS_GUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ALS_GUI
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QRadioButton *radioButton_NW;
    QRadioButton *radioButton_MSA;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_TrainingFile;
    QLineEdit *lineEdit_TrainingFile;
    QPushButton *pushButton_selectTrainingFile;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_InputFile;
    QLineEdit *lineEdit_InputFile;
    QPushButton *pushButton_selectInputFile;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_OutputFile;
    QLineEdit *lineEdit_OutputFile;
    QPushButton *pushButton_selectOutputFile;
    QPushButton *pushButton_ALIGN;
    QDockWidget *dockWidget_MSAParams;
    QWidget *dockWidget_MSAParams_Contents;
    QVBoxLayout *verticalLayout_2;
    QFormLayout *MSAParams_form;
    QLabel *label_emissionPseudocount;
    QLineEdit *lineEdit_emissionPseudocount;
    QLabel *label_transitionPseudocount;
    QLineEdit *lineEdit_transitionPseudocount;
    QLabel *label_columnRemovalThreshold;
    QLineEdit *lineEdit_columnRemovalThreshold;
    QButtonGroup *buttonGroup_AlgoSelect;

    void setupUi(QMainWindow *ALS_GUI)
    {
        if (ALS_GUI->objectName().isEmpty())
            ALS_GUI->setObjectName(QString::fromUtf8("ALS_GUI"));
        ALS_GUI->resize(914, 173);
        centralwidget = new QWidget(ALS_GUI);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetMinimumSize);
        horizontalLayout->setContentsMargins(-1, -1, 0, -1);
        radioButton_NW = new QRadioButton(centralwidget);
        buttonGroup_AlgoSelect = new QButtonGroup(ALS_GUI);
        buttonGroup_AlgoSelect->setObjectName(QString::fromUtf8("buttonGroup_AlgoSelect"));
        buttonGroup_AlgoSelect->addButton(radioButton_NW);
        radioButton_NW->setObjectName(QString::fromUtf8("radioButton_NW"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(radioButton_NW->sizePolicy().hasHeightForWidth());
        radioButton_NW->setSizePolicy(sizePolicy);
        radioButton_NW->setChecked(true);

        horizontalLayout->addWidget(radioButton_NW);

        radioButton_MSA = new QRadioButton(centralwidget);
        buttonGroup_AlgoSelect->addButton(radioButton_MSA);
        radioButton_MSA->setObjectName(QString::fromUtf8("radioButton_MSA"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(radioButton_MSA->sizePolicy().hasHeightForWidth());
        radioButton_MSA->setSizePolicy(sizePolicy1);

        horizontalLayout->addWidget(radioButton_MSA);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setSizeConstraint(QLayout::SetMinimumSize);
        label_TrainingFile = new QLabel(centralwidget);
        label_TrainingFile->setObjectName(QString::fromUtf8("label_TrainingFile"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(label_TrainingFile->sizePolicy().hasHeightForWidth());
        label_TrainingFile->setSizePolicy(sizePolicy2);

        horizontalLayout_3->addWidget(label_TrainingFile);

        lineEdit_TrainingFile = new QLineEdit(centralwidget);
        lineEdit_TrainingFile->setObjectName(QString::fromUtf8("lineEdit_TrainingFile"));
        sizePolicy1.setHeightForWidth(lineEdit_TrainingFile->sizePolicy().hasHeightForWidth());
        lineEdit_TrainingFile->setSizePolicy(sizePolicy1);

        horizontalLayout_3->addWidget(lineEdit_TrainingFile);

        pushButton_selectTrainingFile = new QPushButton(centralwidget);
        pushButton_selectTrainingFile->setObjectName(QString::fromUtf8("pushButton_selectTrainingFile"));

        horizontalLayout_3->addWidget(pushButton_selectTrainingFile);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setSizeConstraint(QLayout::SetMinimumSize);
        label_InputFile = new QLabel(centralwidget);
        label_InputFile->setObjectName(QString::fromUtf8("label_InputFile"));
        sizePolicy2.setHeightForWidth(label_InputFile->sizePolicy().hasHeightForWidth());
        label_InputFile->setSizePolicy(sizePolicy2);

        horizontalLayout_4->addWidget(label_InputFile);

        lineEdit_InputFile = new QLineEdit(centralwidget);
        lineEdit_InputFile->setObjectName(QString::fromUtf8("lineEdit_InputFile"));

        horizontalLayout_4->addWidget(lineEdit_InputFile);

        pushButton_selectInputFile = new QPushButton(centralwidget);
        pushButton_selectInputFile->setObjectName(QString::fromUtf8("pushButton_selectInputFile"));

        horizontalLayout_4->addWidget(pushButton_selectInputFile);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetMinimumSize);
        label_OutputFile = new QLabel(centralwidget);
        label_OutputFile->setObjectName(QString::fromUtf8("label_OutputFile"));
        sizePolicy2.setHeightForWidth(label_OutputFile->sizePolicy().hasHeightForWidth());
        label_OutputFile->setSizePolicy(sizePolicy2);

        horizontalLayout_2->addWidget(label_OutputFile);

        lineEdit_OutputFile = new QLineEdit(centralwidget);
        lineEdit_OutputFile->setObjectName(QString::fromUtf8("lineEdit_OutputFile"));

        horizontalLayout_2->addWidget(lineEdit_OutputFile);

        pushButton_selectOutputFile = new QPushButton(centralwidget);
        pushButton_selectOutputFile->setObjectName(QString::fromUtf8("pushButton_selectOutputFile"));

        horizontalLayout_2->addWidget(pushButton_selectOutputFile);


        verticalLayout->addLayout(horizontalLayout_2);

        pushButton_ALIGN = new QPushButton(centralwidget);
        pushButton_ALIGN->setObjectName(QString::fromUtf8("pushButton_ALIGN"));
        sizePolicy1.setHeightForWidth(pushButton_ALIGN->sizePolicy().hasHeightForWidth());
        pushButton_ALIGN->setSizePolicy(sizePolicy1);

        verticalLayout->addWidget(pushButton_ALIGN);

        ALS_GUI->setCentralWidget(centralwidget);
        dockWidget_MSAParams = new QDockWidget(ALS_GUI);
        dockWidget_MSAParams->setObjectName(QString::fromUtf8("dockWidget_MSAParams"));
        dockWidget_MSAParams_Contents = new QWidget();
        dockWidget_MSAParams_Contents->setObjectName(QString::fromUtf8("dockWidget_MSAParams_Contents"));
        verticalLayout_2 = new QVBoxLayout(dockWidget_MSAParams_Contents);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        MSAParams_form = new QFormLayout();
        MSAParams_form->setObjectName(QString::fromUtf8("MSAParams_form"));
        MSAParams_form->setFormAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_emissionPseudocount = new QLabel(dockWidget_MSAParams_Contents);
        label_emissionPseudocount->setObjectName(QString::fromUtf8("label_emissionPseudocount"));

        MSAParams_form->setWidget(0, QFormLayout::LabelRole, label_emissionPseudocount);

        lineEdit_emissionPseudocount = new QLineEdit(dockWidget_MSAParams_Contents);
        lineEdit_emissionPseudocount->setObjectName(QString::fromUtf8("lineEdit_emissionPseudocount"));

        MSAParams_form->setWidget(0, QFormLayout::FieldRole, lineEdit_emissionPseudocount);

        label_transitionPseudocount = new QLabel(dockWidget_MSAParams_Contents);
        label_transitionPseudocount->setObjectName(QString::fromUtf8("label_transitionPseudocount"));

        MSAParams_form->setWidget(1, QFormLayout::LabelRole, label_transitionPseudocount);

        lineEdit_transitionPseudocount = new QLineEdit(dockWidget_MSAParams_Contents);
        lineEdit_transitionPseudocount->setObjectName(QString::fromUtf8("lineEdit_transitionPseudocount"));

        MSAParams_form->setWidget(1, QFormLayout::FieldRole, lineEdit_transitionPseudocount);

        label_columnRemovalThreshold = new QLabel(dockWidget_MSAParams_Contents);
        label_columnRemovalThreshold->setObjectName(QString::fromUtf8("label_columnRemovalThreshold"));

        MSAParams_form->setWidget(2, QFormLayout::LabelRole, label_columnRemovalThreshold);

        lineEdit_columnRemovalThreshold = new QLineEdit(dockWidget_MSAParams_Contents);
        lineEdit_columnRemovalThreshold->setObjectName(QString::fromUtf8("lineEdit_columnRemovalThreshold"));

        MSAParams_form->setWidget(2, QFormLayout::FieldRole, lineEdit_columnRemovalThreshold);


        verticalLayout_2->addLayout(MSAParams_form);

        dockWidget_MSAParams->setWidget(dockWidget_MSAParams_Contents);
        ALS_GUI->addDockWidget(Qt::RightDockWidgetArea, dockWidget_MSAParams);

        retranslateUi(ALS_GUI);

        QMetaObject::connectSlotsByName(ALS_GUI);
    } // setupUi

    void retranslateUi(QMainWindow *ALS_GUI)
    {
        ALS_GUI->setWindowTitle(QCoreApplication::translate("ALS_GUI", "MainWindow", nullptr));
        radioButton_NW->setText(QCoreApplication::translate("ALS_GUI", "Needleman-Wunsch", nullptr));
        radioButton_MSA->setText(QCoreApplication::translate("ALS_GUI", "MSA", nullptr));
        label_TrainingFile->setText(QCoreApplication::translate("ALS_GUI", "Training file:", nullptr));
        pushButton_selectTrainingFile->setText(QCoreApplication::translate("ALS_GUI", "Select", nullptr));
        label_InputFile->setText(QCoreApplication::translate("ALS_GUI", "Input file:", nullptr));
        pushButton_selectInputFile->setText(QCoreApplication::translate("ALS_GUI", "Select", nullptr));
        label_OutputFile->setText(QCoreApplication::translate("ALS_GUI", "Output file:", nullptr));
        pushButton_selectOutputFile->setText(QCoreApplication::translate("ALS_GUI", "Select", nullptr));
        pushButton_ALIGN->setText(QCoreApplication::translate("ALS_GUI", "ALIGN", nullptr));
        label_emissionPseudocount->setText(QCoreApplication::translate("ALS_GUI", "Emission Pseudocount", nullptr));
        lineEdit_emissionPseudocount->setText(QCoreApplication::translate("ALS_GUI", "1", nullptr));
        lineEdit_emissionPseudocount->setPlaceholderText(QCoreApplication::translate("ALS_GUI", "5%", nullptr));
        label_transitionPseudocount->setText(QCoreApplication::translate("ALS_GUI", "Transmission Pseudocount", nullptr));
        lineEdit_transitionPseudocount->setText(QCoreApplication::translate("ALS_GUI", "1", nullptr));
        label_columnRemovalThreshold->setText(QCoreApplication::translate("ALS_GUI", "Column Removal Threshold", nullptr));
        lineEdit_columnRemovalThreshold->setText(QCoreApplication::translate("ALS_GUI", "21%", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ALS_GUI: public Ui_ALS_GUI {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ALS_GUI_H
