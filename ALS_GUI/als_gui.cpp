#include <QFileDialog>
#include <QMessageBox>
#include "als_gui.h"
#include "ui_als_gui.h"

ALS_GUI::ALS_GUI(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ALS_GUI)
{
    ui->setupUi(this);

    connect(ui->pushButton_selectTrainingFile, &QPushButton::clicked, this, &ALS_GUI::selectTrainingFile);
    connect(ui->pushButton_selectInputFile, &QPushButton::clicked, this, &ALS_GUI::selectInputFile);
    connect(ui->pushButton_selectOutputFile, &QPushButton::clicked, this, &ALS_GUI::selectOutputFile);
    connect(ui->pushButton_ALIGN, &QPushButton::clicked, this, &ALS_GUI::align);

    // Set default files
    this->ui->lineEdit_TrainingFile->setText("/media/kikyy_99/HDD1/FaksHub/2021_2022/DiplProjekt/multiple-sequence-alignment-masters-project/Sandbox/HIV1_REF_2010_genome_DNA.fasta");
    this->ui->lineEdit_InputFile->setText("/media/kikyy_99/HDD1/FaksHub/2021_2022/DiplProjekt/multiple-sequence-alignment-masters-project/Sandbox/ToAlignSequences5_HIVs.multifasta");
    this->ui->lineEdit_OutputFile->setText("/media/kikyy_99/HDD1/FaksHub/2021_2022/DiplProjekt/multiple-sequence-alignment-masters-project/Sandbox/output_file.txt");

    this->openDefaultFiles();
}

void ALS_GUI::openDefaultFiles() {
    // Open default files at start
    QString tmpFileName;

    tmpFileName = this->ui->lineEdit_TrainingFile->text();
    this->trainingFile.setFileName(tmpFileName);

    tmpFileName = this->ui->lineEdit_InputFile->text();
    this->inputFile.setFileName(tmpFileName);

    tmpFileName = this->ui->lineEdit_OutputFile->text();
    this->outputFile.setFileName(tmpFileName);
}

ALS_GUI::~ALS_GUI()
{
    delete ui;
}

void ALS_GUI::selectFile(QFile &fileToSelect, bool readOnly)
{
    if(!readOnly && (&fileToSelect != &this->outputFile)) {
        throw std::invalid_argument("Writing to input or training files!?!");
    }

    QString fileName;
    if(readOnly) {
        fileName = QFileDialog::getOpenFileName(this, "Open the file");
    } else {
        fileName = QFileDialog::getSaveFileName(this, "Save the file");
    }

    if(fileName.isEmpty()) {
        fileToSelect.setFileName("");
//        QMessageBox::warning(this, "Warning", "Couldn't select the desired file.");
        return;
    }

    fileToSelect.setFileName(fileName);
    QMessageBox::information(this, "Info", "File " + fileToSelect.fileName() + " successfully selected!");
}

void ALS_GUI::selectTrainingFile()
{
    this->selectFile(this->trainingFile, true);
    this->ui->lineEdit_TrainingFile->setText(this->trainingFile.fileName());
}

void ALS_GUI::selectInputFile()
{
    this->selectFile(this->inputFile, true);
    this->ui->lineEdit_InputFile->setText(this->inputFile.fileName());
}

void ALS_GUI::selectOutputFile()
{
//    this->inputFileName = this->openFile();
    this->selectFile(this->outputFile, false);
    this->ui->lineEdit_OutputFile->setText(this->outputFile.fileName());
}

bool ALS_GUI::userInput2Double(QString in, double *result, bool percentage) {
    // Remove percentage character '%'
    if(percentage && in.endsWith('%')) {
        in.chop(1);
    }

    // Convert to double, check success
    bool tmpOk;
    double res = in.toDouble(&tmpOk);

    if(percentage && (res < 0 || res > 100)) {
        tmpOk = false;
    }

    if(!tmpOk) {
        // Issue error and return
        QMessageBox::warning(this, "Warning", "Invalid input!");
        res = 0;
    }

    if(result != nullptr) {
        *result = res;
    }

    return tmpOk;
}

/**
 * Check if the input is valid, if not issue a warning message
 */
void ALS_GUI::on_lineEdit_emissionPseudocount_returnPressed()
{
    this->userInput2Double(this->ui->lineEdit_emissionPseudocount->text(), nullptr, false);
}

/**
 * Check if the input is valid, if not issue a warning message
 */
void ALS_GUI::on_lineEdit_transitionPseudocount_returnPressed()
{
    this->userInput2Double(this->ui->lineEdit_transitionPseudocount->text(), nullptr, false);
}

/**
 * Check if the input is valid, if not issue a warning message
 */
void ALS_GUI::on_lineEdit_columnRemovalThreshold_returnPressed()
{
    this->userInput2Double(this->ui->lineEdit_columnRemovalThreshold->text(), nullptr, true);
}
