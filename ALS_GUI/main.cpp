#include "als_gui.h"
#include <QApplication>
using namespace std;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ALS_GUI w;
    w.show();
    return a.exec();
}

//    while(true) {
//        try {
//            QApplication a(argc, argv);
//            ALS_GUI w;
//            w.show();
//            a.exec();
//        } catch(const std::exception& e) {
//            cerr << "Exception occurred!" << endl;
//            cerr << e.what() << endl;
//
//            QMessageBox::warning(nullptr, "Warning", "Invalid input!");
//        }
//    }