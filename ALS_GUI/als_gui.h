#ifndef ALS_GUI_H
#define ALS_GUI_H

#include <QMainWindow>
#include <QFile>

namespace Ui {
class ALS_GUI;
}

class ALS_GUI : public QMainWindow
{
    Q_OBJECT

public:
    explicit ALS_GUI(QWidget *parent = nullptr);
    ~ALS_GUI() override;

private slots:
    void selectTrainingFile();
    void selectInputFile();
    void selectOutputFile();
    void align();

    void on_lineEdit_emissionPseudocount_returnPressed();
    void on_lineEdit_transitionPseudocount_returnPressed();
    void on_lineEdit_columnRemovalThreshold_returnPressed();

private:
    Ui::ALS_GUI *ui;

    std::vector<std::list<char>> trainingSequences{};
    std::vector<std::list<char>> inputSequences{};

    void selectFile(QFile &fileToSelect, bool opening);

    QFile trainingFile{};
    QFile inputFile{};
    QFile outputFile{};

    void openDefaultFiles();
    bool userInput2Double(QString in, double* result, bool percentage = false);
};

#endif // ALS_GUI_H
