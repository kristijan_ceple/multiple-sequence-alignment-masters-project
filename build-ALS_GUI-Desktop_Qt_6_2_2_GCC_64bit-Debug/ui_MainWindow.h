/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 6.2.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ALS_MainWindow
{
public:
    QWidget *ALS_centralwidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QRadioButton *radioButton_NW;
    QRadioButton *radioButton_MSA;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_OutputFile;
    QLineEdit *lineEdit_OutputFile;
    QPushButton *pushButton_selectOutputFile;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_TrainingFile;
    QLineEdit *lineEdit_TrainingFile;
    QPushButton *pushButton_selectTrainingFile;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_InputFile;
    QLineEdit *lineEdit_InputFile;
    QPushButton *pushButton_selectInputFile;
    QPushButton *pushButton_ALIGN;
    QDockWidget *dockWidget_MSAParams;
    QWidget *dockWidge_MSAParams_tContents;
    QVBoxLayout *verticalLayout_2;
    QFormLayout *MSAParams_form;
    QLabel *label_emissionPseudocount;
    QLineEdit *lineEdit_emissionPseudocountPercentage;
    QLabel *label_transmissionPseudocount;
    QLineEdit *lineEdit_transmissionPseudocountPercentage;
    QLabel *label_columnRemovalThreshold;
    QLineEdit *lineEdit_columnRemovalThreshold;

    void setupUi(QMainWindow *ALS_MainWindow)
    {
        if (ALS_MainWindow->objectName().isEmpty())
            ALS_MainWindow->setObjectName(QString::fromUtf8("ALS_MainWindow"));
        ALS_MainWindow->resize(1029, 201);
        ALS_centralwidget = new QWidget(ALS_MainWindow);
        ALS_centralwidget->setObjectName(QString::fromUtf8("ALS_centralwidget"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ALS_centralwidget->sizePolicy().hasHeightForWidth());
        ALS_centralwidget->setSizePolicy(sizePolicy);
        verticalLayout = new QVBoxLayout(ALS_centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetMinimumSize);
        horizontalLayout->setContentsMargins(-1, -1, 0, -1);
        radioButton_NW = new QRadioButton(ALS_centralwidget);
        radioButton_NW->setObjectName(QString::fromUtf8("radioButton_NW"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(radioButton_NW->sizePolicy().hasHeightForWidth());
        radioButton_NW->setSizePolicy(sizePolicy1);
        radioButton_NW->setChecked(true);

        horizontalLayout->addWidget(radioButton_NW);

        radioButton_MSA = new QRadioButton(ALS_centralwidget);
        radioButton_MSA->setObjectName(QString::fromUtf8("radioButton_MSA"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(radioButton_MSA->sizePolicy().hasHeightForWidth());
        radioButton_MSA->setSizePolicy(sizePolicy2);

        horizontalLayout->addWidget(radioButton_MSA);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetMinimumSize);
        label_OutputFile = new QLabel(ALS_centralwidget);
        label_OutputFile->setObjectName(QString::fromUtf8("label_OutputFile"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(label_OutputFile->sizePolicy().hasHeightForWidth());
        label_OutputFile->setSizePolicy(sizePolicy3);

        horizontalLayout_2->addWidget(label_OutputFile);

        lineEdit_OutputFile = new QLineEdit(ALS_centralwidget);
        lineEdit_OutputFile->setObjectName(QString::fromUtf8("lineEdit_OutputFile"));

        horizontalLayout_2->addWidget(lineEdit_OutputFile);

        pushButton_selectOutputFile = new QPushButton(ALS_centralwidget);
        pushButton_selectOutputFile->setObjectName(QString::fromUtf8("pushButton_selectOutputFile"));

        horizontalLayout_2->addWidget(pushButton_selectOutputFile);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setSizeConstraint(QLayout::SetMinimumSize);
        label_TrainingFile = new QLabel(ALS_centralwidget);
        label_TrainingFile->setObjectName(QString::fromUtf8("label_TrainingFile"));
        sizePolicy3.setHeightForWidth(label_TrainingFile->sizePolicy().hasHeightForWidth());
        label_TrainingFile->setSizePolicy(sizePolicy3);

        horizontalLayout_3->addWidget(label_TrainingFile);

        lineEdit_TrainingFile = new QLineEdit(ALS_centralwidget);
        lineEdit_TrainingFile->setObjectName(QString::fromUtf8("lineEdit_TrainingFile"));
        sizePolicy2.setHeightForWidth(lineEdit_TrainingFile->sizePolicy().hasHeightForWidth());
        lineEdit_TrainingFile->setSizePolicy(sizePolicy2);

        horizontalLayout_3->addWidget(lineEdit_TrainingFile);

        pushButton_selectTrainingFile = new QPushButton(ALS_centralwidget);
        pushButton_selectTrainingFile->setObjectName(QString::fromUtf8("pushButton_selectTrainingFile"));

        horizontalLayout_3->addWidget(pushButton_selectTrainingFile);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setSizeConstraint(QLayout::SetMinimumSize);
        label_InputFile = new QLabel(ALS_centralwidget);
        label_InputFile->setObjectName(QString::fromUtf8("label_InputFile"));
        sizePolicy3.setHeightForWidth(label_InputFile->sizePolicy().hasHeightForWidth());
        label_InputFile->setSizePolicy(sizePolicy3);

        horizontalLayout_4->addWidget(label_InputFile);

        lineEdit_InputFile = new QLineEdit(ALS_centralwidget);
        lineEdit_InputFile->setObjectName(QString::fromUtf8("lineEdit_InputFile"));

        horizontalLayout_4->addWidget(lineEdit_InputFile);

        pushButton_selectInputFile = new QPushButton(ALS_centralwidget);
        pushButton_selectInputFile->setObjectName(QString::fromUtf8("pushButton_selectInputFile"));

        horizontalLayout_4->addWidget(pushButton_selectInputFile);


        verticalLayout->addLayout(horizontalLayout_4);

        pushButton_ALIGN = new QPushButton(ALS_centralwidget);
        pushButton_ALIGN->setObjectName(QString::fromUtf8("pushButton_ALIGN"));
        sizePolicy2.setHeightForWidth(pushButton_ALIGN->sizePolicy().hasHeightForWidth());
        pushButton_ALIGN->setSizePolicy(sizePolicy2);

        verticalLayout->addWidget(pushButton_ALIGN);

        ALS_MainWindow->setCentralWidget(ALS_centralwidget);
        dockWidget_MSAParams = new QDockWidget(ALS_MainWindow);
        dockWidget_MSAParams->setObjectName(QString::fromUtf8("dockWidget_MSAParams"));
        dockWidge_MSAParams_tContents = new QWidget();
        dockWidge_MSAParams_tContents->setObjectName(QString::fromUtf8("dockWidge_MSAParams_tContents"));
        verticalLayout_2 = new QVBoxLayout(dockWidge_MSAParams_tContents);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        MSAParams_form = new QFormLayout();
        MSAParams_form->setObjectName(QString::fromUtf8("MSAParams_form"));
        MSAParams_form->setFormAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_emissionPseudocount = new QLabel(dockWidge_MSAParams_tContents);
        label_emissionPseudocount->setObjectName(QString::fromUtf8("label_emissionPseudocount"));

        MSAParams_form->setWidget(0, QFormLayout::LabelRole, label_emissionPseudocount);

        lineEdit_emissionPseudocountPercentage = new QLineEdit(dockWidge_MSAParams_tContents);
        lineEdit_emissionPseudocountPercentage->setObjectName(QString::fromUtf8("lineEdit_emissionPseudocountPercentage"));

        MSAParams_form->setWidget(0, QFormLayout::FieldRole, lineEdit_emissionPseudocountPercentage);

        label_transmissionPseudocount = new QLabel(dockWidge_MSAParams_tContents);
        label_transmissionPseudocount->setObjectName(QString::fromUtf8("label_transmissionPseudocount"));

        MSAParams_form->setWidget(1, QFormLayout::LabelRole, label_transmissionPseudocount);

        lineEdit_transmissionPseudocountPercentage = new QLineEdit(dockWidge_MSAParams_tContents);
        lineEdit_transmissionPseudocountPercentage->setObjectName(QString::fromUtf8("lineEdit_transmissionPseudocountPercentage"));

        MSAParams_form->setWidget(1, QFormLayout::FieldRole, lineEdit_transmissionPseudocountPercentage);

        label_columnRemovalThreshold = new QLabel(dockWidge_MSAParams_tContents);
        label_columnRemovalThreshold->setObjectName(QString::fromUtf8("label_columnRemovalThreshold"));

        MSAParams_form->setWidget(2, QFormLayout::LabelRole, label_columnRemovalThreshold);

        lineEdit_columnRemovalThreshold = new QLineEdit(dockWidge_MSAParams_tContents);
        lineEdit_columnRemovalThreshold->setObjectName(QString::fromUtf8("lineEdit_columnRemovalThreshold"));

        MSAParams_form->setWidget(2, QFormLayout::FieldRole, lineEdit_columnRemovalThreshold);


        verticalLayout_2->addLayout(MSAParams_form);

        dockWidget_MSAParams->setWidget(dockWidge_MSAParams_tContents);
        ALS_MainWindow->addDockWidget(Qt::RightDockWidgetArea, dockWidget_MSAParams);

        retranslateUi(ALS_MainWindow);

        QMetaObject::connectSlotsByName(ALS_MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *ALS_MainWindow)
    {
        ALS_MainWindow->setWindowTitle(QCoreApplication::translate("ALS_MainWindow", "MainWindow", nullptr));
        radioButton_NW->setText(QCoreApplication::translate("ALS_MainWindow", "Needleman-Wunsch", nullptr));
        radioButton_MSA->setText(QCoreApplication::translate("ALS_MainWindow", "MSA", nullptr));
        label_OutputFile->setText(QCoreApplication::translate("ALS_MainWindow", "Output file:", nullptr));
        pushButton_selectOutputFile->setText(QCoreApplication::translate("ALS_MainWindow", "Select", nullptr));
        label_TrainingFile->setText(QCoreApplication::translate("ALS_MainWindow", "Training file:", nullptr));
        pushButton_selectTrainingFile->setText(QCoreApplication::translate("ALS_MainWindow", "Select", nullptr));
        label_InputFile->setText(QCoreApplication::translate("ALS_MainWindow", "Input file:", nullptr));
        pushButton_selectInputFile->setText(QCoreApplication::translate("ALS_MainWindow", "Select", nullptr));
        pushButton_ALIGN->setText(QCoreApplication::translate("ALS_MainWindow", "ALIGN", nullptr));
        label_emissionPseudocount->setText(QCoreApplication::translate("ALS_MainWindow", "Emission Pseudocount", nullptr));
        lineEdit_emissionPseudocountPercentage->setPlaceholderText(QCoreApplication::translate("ALS_MainWindow", "5%", nullptr));
        label_transmissionPseudocount->setText(QCoreApplication::translate("ALS_MainWindow", "Transmission Pseudocount", nullptr));
        lineEdit_transmissionPseudocountPercentage->setText(QCoreApplication::translate("ALS_MainWindow", "5%", nullptr));
        label_columnRemovalThreshold->setText(QCoreApplication::translate("ALS_MainWindow", "Column Removal Threshold", nullptr));
        lineEdit_columnRemovalThreshold->setText(QCoreApplication::translate("ALS_MainWindow", "21%", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ALS_MainWindow: public Ui_ALS_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
