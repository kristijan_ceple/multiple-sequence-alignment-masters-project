//
// Created by kikyy_99 on 07.10.21.
//

#include "MyUtil.h"
#include <fstream>
#include <boost/algorithm/string.hpp>
using namespace std;

std::vector<std::string> MyUtil::IOOps::readFile(const std::string& filename) {
    vector<string> toRet{};
    ifstream readFile{filename};

    if(!readFile.is_open()) {
        throw runtime_error("Couldn't open file");
    }

    string line, tmp;
    // Prepare first line/sequence!
    getline(readFile, line);
    boost::trim(line);
    if(line.starts_with(">")) {
        tmp = "";
    } else {
        tmp = move(line);
    }

    while(getline(readFile, line)) {
        boost::trim(line);

        // For each line - check whether it's a comment or not
        if(line.starts_with(">")) {
            // Comment, can skip.
            toRet.emplace_back(std::move(tmp));
            tmp = "";
        } else {
            tmp += line;
        }
    }

    // Add last sequence!
    toRet.emplace_back(std::move(tmp));

    return toRet;
}

vector<list<char>> MyUtil::IOOps::readSequencesFile(const std::string& filename) {
    vector<string> file = IOOps::readFile(filename);
    vector<list<char>> toRet{};

    for(const auto& seq : file) {
        toRet.emplace_back(seq.cbegin(), seq.cend());
    }

    return toRet;
}
