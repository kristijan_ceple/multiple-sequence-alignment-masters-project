//
// Created by kikyy_99 on 06.10.21.
//

#ifndef MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_PROFILEHMM_H
#define MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_PROFILEHMM_H


#include <list>
#include <vector>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <boost/container_hash/hash.hpp>

class ProfileHMM {
    friend class ProgressiveMSA_HMM_Alignment_Algorithm;
    friend class Viterbi;

private:
    static inline constexpr int HMM_OUTGOING_TRANSITIONS = 3;

    struct State {
        enum class Type: uint8_t {I, M, D, S, E};
        const int index;
        const Type type;

        State() = delete;
        State(int index, Type type) : index{index}, type{type} {};
        State(const State& other) = default;        // Copy constructor

        auto operator<=>(const State&) const = default;
        friend std::ostream& operator<<(std::ostream& os, const State& inState);
//        bool operator==(const HMMState& other) const {
//            return (this->index == other.index) && (this->type == other.type);
//        }

        // Copy assignment operator
//        HMMState& operator=(const HMMState& other) = default;

        // Move assignment operator
//        HMMState& operator=(HMMState&& other) noexcept = default;

        class HashFunction {
        public:
            size_t operator()(const State& s) const {
                size_t seed = 0;
                boost::hash_combine(seed, s.index);
                boost::hash_combine(seed, s.type);
                return seed;
            }
        };
    };

    class LDCounter {
    friend class ProfileHMM;
    private:
        int letterCount = 0, dashCount = 0;
        std::unordered_map<char, int> letterFreqs;

        const ProfileHMM& HMM;
        std::vector<std::list<char>::const_iterator> columnTrackers;
    public:
        explicit LDCounter(const ProfileHMM& HMM);
        LDCounter(const LDCounter&) = default;      // Copy constructor
        void operator()();
        void clearAndSync();
        void addLetter(char toAdd);

        LDCounter& operator++();
        LDCounter& operator--();
//        const LDCounter operator++(int);
//        const LDCounter operator--(int);
    };

    static inline int idCounter = 0;
    const int id;

    size_t numOfCols, numOfRows;
    const std::vector<std::list<char>>& MSAFull;
    std::vector<std::reference_wrapper<const std::list<char>>> MSAForHMMBuilding;

    std::vector<std::list<char>::const_iterator> MSATrackers;
    std::vector<int> conservedRegions;
    std::unordered_set<char> charactersSet;

    std::unordered_map<char, int> charsFreqBlueprint;
    std::unordered_map<State, std::unordered_map<State::Type, double>, State::HashFunction> transitionsMap;
    std::unordered_map<State, std::unordered_map<char, double>, State::HashFunction> emissionsMap;

    void initParameters();
    void makeCharsSet();
    void filterConservedRegions();
    void calculateProbabilities();
//    void endCalc(LDCounter& transLDCounter, LDCounter& transNextLDCounter, LDCounter& emitLDCounter, int currCol);
    double PSEUDOCOUNT_TRANSITION_DENOMINATOR, PSEUDOCOUNT_EMISSION_DENOMINATOR;

    friend std::ostream& operator<<(std::ostream& os, const ProfileHMM& phmm);
    friend std::ostream& operator<<(std::ostream& os, const State& inState);
    friend std::ostream& operator<<(std::ostream& os, State::Type inType);
protected:
    void updateParameters();              // TODO: Implement this in a computationally more efficient way

public:
    const double PSEUDOCOUNT_EMISSION, PSEUDOCOUNT_TRANSITION, COLUMN_REMOVAL_THRESHOLD;
    inline size_t getConservedRegionSize() const { return this->conservedRegions.size(); };
    inline const std::vector<int>& getConservedRegions() const {return this->conservedRegions;};

    static inline char ProfileHMMStateType2Char(ProfileHMM::State::Type in);

    void addLastConservedRegion();
    void removeLastConservedRegion();

    explicit ProfileHMM(const std::vector<std::list<char>>& MSA, int numberOfSeqsToBuildHMMFor,
                        double PSEUDOCOUNT_EMISSION = 0.05, double PSEUDOCOUNT_TRANSITION = 0.05, double COLUMN_REMOVAL_THRESHOLD = 0.21);
};


#endif //MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_PROFILEHMM_H
