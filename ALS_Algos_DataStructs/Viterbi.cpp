//
// Created by kikyy_99 on 15. 01. 2022..
//

#include "Viterbi.h"
#include <execution>

Viterbi::ViterbiGraphCell::ViterbiGraphCell(VGState cellState, int stateIndex, double cellVal)
: cellState{cellState}, stateIndex{stateIndex}, cellVal{cellVal}
{
    if((std::isnan(cellVal))) {
        this->cellVal = -1.*INFINITY;     // TODO: Check this part later whether we really want to NAN -> -Infinity
    }
}

Viterbi::Viterbi(const ProfileHMM &profileHMM) : profileHMM{&profileHMM} {
    this->fillLogMaps();
}

void Viterbi::fillLogMaps() {
    // First logTransitionProbs
    std::for_each(
    std::execution::par_unseq,
    this->profileHMM->transitionsMap.begin(), this->profileHMM->transitionsMap.end(),
    [this](auto& entry) {
            std::unordered_map<ProfileHMM::State::Type, double> toFill{};

            std::for_each(
                    std::execution::par_unseq,
                    entry.second.begin(), entry.second.end(),
                    [&toFill](auto& subEntry) {
                        toFill.emplace(subEntry.first, log(subEntry.second));
                    }
            );

            this->logTransitionProbs.emplace(entry.first, toFill);
        }
    );

    // Then logEmissionProbs
    std::for_each(
        std::execution::par_unseq,
        this->profileHMM->emissionsMap.begin(), this->profileHMM->emissionsMap.end(),
        [this] (auto& entry) {
            std::unordered_map<char, double> toFill{};

            std::for_each(
                std::execution::par_unseq,
                entry.second.begin(), entry.second.end(),
                [&toFill](auto& subEntry) {
                    toFill.emplace(subEntry.first, log(subEntry.second));
                }
            );

            this->logEmissionProbs.emplace(entry.first, toFill);
        }
    );
}

std::vector<Viterbi::ViterbiGraphCell::ViterbiGraphState> Viterbi::getViterbiPath(std::list<char> &sequence) {
    // "S" + sequence itself in order to simplify the Viterbi Graph construction
    sequence.emplace_front('S');
    this->sequenceToAlign = &sequence;
    std::vector<VGState> toRet;
    toRet.reserve(sequence.size());

    int seqLen = this->sequenceToAlign->size();
    int m = this->profileHMM->getConservedRegionSize();
    // Pripremiti tablicu proslih stanja - za sada znamo prvi redak i stupac
    this->VGTable = Array2DOwner<ViterbiGraphCell>(new ViterbiGraphCell[(m+1) * seqLen], m+1, seqLen);

    this->viterbiGraph();
    this->traceback(toRet);

    sequence.pop_front();                   // Remove the 'S'
    this->sequenceToAlign = nullptr;        // Reset the object member
    return toRet;
}

void Viterbi::viterbiGraph() {
    int seqLen = this->sequenceToAlign->size();
    int m = this->profileHMM->getConservedRegionSize();

    /*
    Jako slicno algoritmu dinamickog programiranja.
    Pomicanje udesno -> Insertion
    Pomicanje dijagonalno -> Match
    Pomicanje prema dolje -> Deletion
    Potrebno doci u skroz donji desni kut(End stanje), krecemo iz gornjeg lijevog(Start stanje)
    Prilikom popunjavanja tablice, moramo voditi brige o trenutnim indeksima. Paziti na to da I ima loopove pa ide
    sam u sebe, a D i M uvijek napreduju trenutan HMM indeks.
    */

    // 1. U Start stanju smo. Idem popuniti prvi redak i prvi stupac sa brojkama
    // Start inicijaliziramo sa kumulativnom vrijednoscu 0
    this->VGTable->at(0, 0) = ViterbiGraphCell(VGState::S, 0, 0);

    double tmpVal, fromUp, fromLeft, fromDiagonal;
    double tmpTransVal;
    char currChar;
    std::unordered_map<char, double>* tmpEmitMap;
    auto seqIt = this->sequenceToAlign->cbegin();
    ++seqIt;

    // Popunimo S -> I0, tj. table[0][1]
    currChar = *(seqIt++);      // .get(1)
    tmpVal = 0
            + (this->logTransitionProbs[HMMState{0, HMMSType::S}][HMMSType::I])
            + (this->logEmissionProbs[HMMState{0, HMMSType::I}][currChar]);

    this->VGTable->at(0, 1) = ViterbiGraphCell{VGState::I, 0, tmpVal};

    // Popunjavamo prvi redak
    tmpTransVal = this->logTransitionProbs[HMMState{0, HMMSType::I}][HMMSType::I];
    tmpEmitMap = &this->logEmissionProbs[HMMState{0, HMMSType::I}];
    auto seqItCpy = seqIt;
    for(int i = 2; i < seqLen; i++) {
        // Gledamo samo udesno, I0 -> I0
        currChar = *(seqItCpy++);     // TODO: Check this iterator
        tmpVal = this->VGTable->at(0, i-1).cellVal + tmpTransVal + (*tmpEmitMap)[currChar];
        this->VGTable->at(0, i) = ViterbiGraphCell{VGState::I, 0, tmpVal};
    }

    // Popunimo S -> D1, i zatim ostatak 1. stupca
    tmpVal = 0 + this->logTransitionProbs[HMMState{0, HMMSType::S}][HMMSType::D] + 0;      // TODO: Is the chance in deletion for emission 100% or 0%???
    this->VGTable->at(1, 0) = ViterbiGraphCell{VGState::D, 1, tmpVal};

    // Sada ostatak 1. stupca - D1 -> D2, D2 -> D3, itd...
    for(int i = 2; i <= m; i++) {
        tmpVal = this->VGTable->at(i-1, 0).cellVal
                + this->logTransitionProbs[HMMState{i - 1, HMMSType::D}][HMMSType::D];
        this->VGTable->at(i, 0) = ViterbiGraphCell{VGState::D, i, tmpVal};
    }

    // Sada ostatak tablice, North-West -> South-East
    ViterbiGraphCell* prevState = nullptr;
    VGState maxState;
    double maxVal;
    int prevStateIndexLeft, prevStateIndexUp, prevStateIndexDiagonal, stateIndex;
    for(int currVGRow = 1; currVGRow <= m; currVGRow++) {
        seqIt = this->sequenceToAlign->cbegin();
        ++seqIt;

        for(int currVGCol = 1; currVGCol < seqLen; currVGCol++) {
            currChar = *(seqIt++);      // .get(1)
            /*
				Sada, idemo popunjavati, mozemo doci iz tri smjera:
					fromUp - dosli od gore, znaci prev_state -> D
					fromLeft - dosli od lijevo, znaci prev_state -> I
					fromDiagonal - dijagonalno, znaci prev_state -> M

					Pri tome treba pratiti indekse za D, I i M. Kako cemo znati koje je
					previous stanje? Trebalo bi nekako i njega(njih) pratiti.

					Treba znati stanja iz retka iznad i trenutnog retka. Indeksi stanja su ovisni o trenutnoj poziciji u
					sekvenci???
				 */
            // Prvo od gore - Deletion
            prevState = &this->VGTable->at(currVGRow-1, currVGCol);
            stateIndex = prevStateIndexUp = prevState->stateIndex;
            fromUp = this->VGTable->at(currVGRow-1, currVGCol).cellVal
                    + this->logTransitionProbs[ViterbiGraphCell::vgc2HMMState(*prevState)][HMMSType::D];

            // Zatim gledajmo od lijevo - Insertion
            prevState = &this->VGTable->at(currVGRow, currVGCol-1);
            stateIndex = prevStateIndexLeft = prevState->stateIndex;
            fromLeft = this->VGTable->at(currVGRow, currVGCol-1).cellVal
                    + this->logTransitionProbs[ViterbiGraphCell::vgc2HMMState(*prevState)][HMMSType::I]
                    + this->logEmissionProbs[HMMState{prevState->stateIndex, HMMSType::I}][currChar];

            // Konacno gledajmo dijagonalno - MiMatch
            prevState = &this->VGTable->at(currVGRow-1, currVGCol-1);
            stateIndex = prevStateIndexDiagonal = prevState->stateIndex;
            fromDiagonal = this->VGTable->at(currVGRow-1, currVGCol-1).cellVal
                    + this->logTransitionProbs[ViterbiGraphCell::vgc2HMMState(*prevState)][HMMSType::M]
                    + this->logEmissionProbs[HMMState{prevState->stateIndex+1, HMMSType::M}][currChar];

            // Sve NAN-ove pretvoriti u -inf
            if(std::isnan(fromUp)) {
                fromUp = -1. * INFINITY;
            }

            if(std::isnan(fromLeft)) {
                fromLeft = -1. * INFINITY;
            }

            if(std::isnan(fromDiagonal)) {
                fromDiagonal = -1. * INFINITY;
            }

            // Sada treba naci max i staviti ga u tablicu
            maxState = VGState::D;
            maxVal = fromUp;
            stateIndex = prevStateIndexUp + 1;

            if(fromLeft > maxVal) {
                maxVal = fromLeft;
                maxState = VGState::I;
                stateIndex = prevStateIndexLeft;
            }

            if(fromDiagonal > maxVal) {
                maxVal = fromDiagonal;
                maxState = VGState::M;
                stateIndex = prevStateIndexDiagonal+1;
            }

            // Staviti najvecega u tablicu
            this->VGTable->at(currVGRow, currVGCol) = ViterbiGraphCell{maxState, stateIndex, maxVal};

            // Nastaviti dalje i tako ici dok ne popunimo tablicu
        }
    }

    // Na kraju ce uvijek otici u End!
}

void Viterbi::traceback(std::vector<VGState> &viterbiPath) {
    viterbiPath.emplace_back(VGState::E);
    int m = this->profileHMM->getConservedRegionSize();
    int rowsNum = m+1;
    int colsNum = this->sequenceToAlign->size();

    int currRow = rowsNum-1;
    int currCol = colsNum-1;

    // Start from the South-Eastern corner, go to Start state
    ViterbiGraphCell* currCell = nullptr;
    VGState currCellState;

    while(true) {
        currCell = &this->VGTable->at(currRow, currCol);
        currCellState = currCell->cellState;

        viterbiPath.emplace_back(currCellState);
        switch(currCellState) {
            case VGState::D:
                // Dosli od gore
                currRow--;
                break;
            case VGState::I:
                // Dosli od lijeva
                currCol--;
                break;
            case VGState::M:
                // Dosli od dijagonalno
                currRow--;
                currCol--;
                break;
            case VGState::S:
                // Gotovo! Samo treba obrnuti put tako da ide od S prema E
                // Maknuti S, nece trebati za ubuduce :O
                viterbiPath.pop_back();
                std::reverse(viterbiPath.begin(), viterbiPath.end());
                return;
        }
    }
}