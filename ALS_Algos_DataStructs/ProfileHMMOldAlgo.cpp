//
// Created by kikyy_99 on 21. 03. 2022..
//

void ProfileHMM::calculateProbabilities2() {
    // This is where the fun begins
    int HMMIndex = 1;
    double denominatorTmp;

    // TODO: Implement cloning here?
    LDCounter transLDCounter{*this};
    LDCounter transNextLDCounter{*this};
    LDCounter emitLDCounter{*this};

    /*
    Pripremiti sva moguca stanja - de fakto HMM = mapa tranzicijskih i emisijskih vjerojatnosti
    m = broj konzerviranih regija
    Start stanje, End stanje
    m Deletion stanja(D1 do Dm)
    m Mimatch stanja(M1 do Mm)
    m+1 Insertion stanja(I0 do Im)
    */

    // Popuniti mapu za emisije
    size_t m = this->conservedRegions.size();

    // ##############################				INIT				############################################
    // Emissions
    // Prvo Mimatch stanja(ima ih m)
    for(int i = 1; i <= m; i++) {
        auto it = this->emissionsMap.emplace(State{i, State::Type::M}, unordered_map<char, double>());
        for(char currChar : this->charactersSet) {
            it.first->second[currChar] = 0.;
        }
    }

    // Emissions
    // Zatim Insertion stanja(ima ih m+1, tj. indeks im krece od 0)
    for(int i = 0; i <= m; i++) {
        auto it = this->emissionsMap.emplace(State{i, State::Type::I}, unordered_map<char, double>());
        for(char currChar : this->charactersSet) {
            it.first->second[currChar] = 0.;
        }
    }

    // Transitions
    // Start i I0
    auto tIt = this->transitionsMap.emplace(State{0, State::Type::S}, unordered_map<State, double, State::HashFunction>());
    tIt.first->second[{1, State::Type::M}] = 0.;
    tIt.first->second[{1, State::Type::D}] = 0.;
    tIt.first->second[{0, State::Type::I}] = 0.;

    // I0 sada
    this->transitionsMap.emplace(State{0, State::Type::I}, tIt.first->second);

    for(int i = 1; i < m; i++) {
        // Di -> D_i+1, Di -> Ii, Di -> M_i+1
        tIt = this->transitionsMap.emplace(State{i, State::Type::D}, unordered_map<State, double, State::HashFunction>());
        tIt.first->second[{i + 1, State::Type::D}] = 0.;
        tIt.first->second[{i, State::Type::I}] = 0.;
        tIt.first->second[{i + 1, State::Type::M}] = 0.;

        // Mi -> M_i+1, Mi -> Ii, Mi -> D_i+1
        this->transitionsMap.emplace(State{i, State::Type::M}, tIt.first->second);

        // Ii -> Ii, Ii -> D_i+1, Ii -> M_i+1
        this->transitionsMap.emplace(State{i, State::Type::I}, tIt.first->second);
    }

    // End i Im - handliranje zadnjeg retka
    // Dm -> E, Dm -> Im ; Mm -> E, Mm -> Im ; Im -> Im, Im -> E
    tIt = this->transitionsMap.emplace(State{static_cast<int>(m), State::Type::D}, unordered_map<State, double, State::HashFunction>());
    tIt.first->second[{static_cast<int>(m), State::Type::E}] = 0.;
    tIt.first->second[{static_cast<int>(m), State::Type::I}] = 0.;

    this->transitionsMap.emplace(State{static_cast<int>(m), State::Type::M}, tIt.first->second);

    this->transitionsMap.emplace(State{static_cast<int>(m), State::Type::I}, tIt.first->second);
    // ##############################				INIT				############################################

    // ##############################			GLAVNI DIO - PROBS CALC			####################################
    bool inConservedRegion;
    bool nextColumnConservedRegion;
    char currChar;

    // Pocetak(stanje S) handlirati rucno - 0. stupac
    int nextConsrvRegNum;
    if(m > 0) {
        nextConsrvRegNum = this->conservedRegions[0];
        inConservedRegion = nextConsrvRegNum == 0;
    } else {
        // TODO: Check this later, might be an edge case that needs to be explicitly calculated
        inConservedRegion = false;
        nextConsrvRegNum = this->MSAForHMMBuilding.front().get().size();            // Put it just past the end of the sequence
    }

    // Prebrojati znakove u nultom stupcu
    transLDCounter();

    // Izracunati vjerojatnosti za S i I(0. stupac sekvence)
    // S -> I0, S -> M1 i S -> D1
    // I0 -> I0, I0 -> D1 i I0 ->M1
    if(inConservedRegion) {
        // Bijela zona, racunamo vjerojatnosti za D1 i M1(za I0 pseudocountovi su vec stavljeni)

        // Tranzicije
        denominatorTmp = (this->numOfRows + this->PSEUDOCOUNT_TRANSITION_DENOMINATOR);
        double transitionToMimatchState = ((double)transLDCounter.letterCount + this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;
        double transitionToDeleteState = ((double)transLDCounter.dashCount + this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;
        double transitionToInsertionState = this->PSEUDOCOUNT_TRANSITION/denominatorTmp;

        tIt = this->transitionsMap.emplace(State{0, State::Type::S}, unordered_map<State, double, State::HashFunction>());
        tIt.first->second[{1, State::Type::M}] = transitionToMimatchState;
        tIt.first->second[{1, State::Type::D}] = transitionToDeleteState;
        tIt.first->second[{0, State::Type::I}] = transitionToInsertionState;

        // Match(osim njega i Insertion) moze emitirati znakove. Ali S ne emitira znakove pa nista tu
    } else {
        // Siva zona - racunamo vjerojatnosti za I0
        // Usli smo u sivu regiju. Pogledajmo na kojem indexu je prva konzervativna regija

        // Pogledati koliko sekvenci unutar sive regije ima barem 1 slovo - to su tranzicije S -> I
        // Ukoliko nema nijednoga slova - pogledati koji je prvi znak u bijeloj zoni
        // Ukoliko je to '-' onda je to tranzicija S -> D, a inace S -> M
        int start2Insertion = 0, start2Deletion = 0, start2Mimatch = 0;
        int insertion2Insertion = 0, insertion2Mimatch = 0, insertion2Deletion = 0;

        for(int currRow = 0; currRow < this->numOfRows; currRow++) {
            bool ideInsertion = false;
            // Looking ahead, row by row -> forward-scan each row
            auto& currSeqIt = this->MSATrackers[currRow];

            for(int currCol = 0; currCol < nextConsrvRegNum; currCol++) {
                currChar = *currSeqIt;
                ++currSeqIt;

                // Provjeravamo Start -> Insertion?
                if(currChar != MyUtil::Algorithm::GAP) {
                    ideInsertion = true;

                    // Pobrojati ovaj znak koji se pojavljuje u I0 stanju
                    emitLDCounter.addLetter(currChar);

                    // Insertion -> Insertion sub-for
                    // Hoce li ponovo otici u Insertion - i koliko puta? Usput potrebno brojati emitirane charove
                    for(++currCol; currCol < nextConsrvRegNum; currCol++) {
                        currChar = *currSeqIt;
                        ++currSeqIt;

                        if(currChar != MyUtil::Algorithm::GAP) {
                            insertion2Insertion++;

                            // Pobrojati ovaj znak koji se pojavljuje u I0 stanju
                            emitLDCounter.addLetter(currChar);
                        }
                    }

                    break;
                }
            }

            // TODO: Check this -> Iterator should be at nextConsrvRegNum
            currChar = *currSeqIt;
            if(ideInsertion) {
                start2Insertion++;

                // Konacno pogledati kamo ce otici poslije svih tih insertion loopova?
                if(currChar != MyUtil::Algorithm::GAP) {
                    insertion2Mimatch++;
                } else {
                    insertion2Deletion++;
                }
            } else {
                // Provjeriti jesmo li otisli u Mimatch ili Deletion
                if(currChar != MyUtil::Algorithm::GAP) {
                    start2Mimatch++;
                } else {
                    start2Deletion++;
                }
            }
        }

        // Izracunati vjerojatnosti tranzicije, staviti ih u mapu, i tu mapu staviti u mapu :D
        denominatorTmp = (this->numOfRows + this->PSEUDOCOUNT_TRANSITION_DENOMINATOR);
        double transitionToMimatchState = ((double)start2Mimatch + this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;
        double transitionToDeleteState = ((double)start2Deletion + this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;
        double transitionToInsertionState = ((double)start2Insertion + this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;

        tIt = this->transitionsMap.emplace(State{0, State::Type::S}, unordered_map<State, double, State::HashFunction>());
        tIt.first->second[{1, State::Type::M}] = transitionToMimatchState;
        tIt.first->second[{1, State::Type::D}] = transitionToDeleteState;
        tIt.first->second[{0, State::Type::I}] = transitionToInsertionState;

        // I0 moze emitirati i znakove, pa treba i to obraditi za emisijske koeficijente
        denominatorTmp = emitLDCounter.letterCount + this->PSEUDOCOUNT_EMISSION_DENOMINATOR;
        auto eIt = this->emissionsMap.emplace(State{0, State::Type::I}, unordered_map<char, double>());
        for(const auto& currEntry : emitLDCounter.letterFreqs) {
            char currSubChar = currEntry.first;
            int currSubCharNum = currEntry.second;

            eIt.first->second[currSubChar] = ((double)currSubCharNum + this->PSEUDOCOUNT_EMISSION)/denominatorTmp;
        }
    }

    // Dial them back because they get incremented in the loop
    for(int i = 0; i < this->MSATrackers.size(); i++) {
        this->MSATrackers[i]--;
    }

    // Sada obradivati ostatak stanja - krenuti ponovo od PRVE bijele regije
    for(int currCol = nextConsrvRegNum; currCol < this->numOfCols; currCol++) {
        for(int i = 0; i < this->MSATrackers.size(); i++) {
            this->MSATrackers[i]++;
        }

        // Na pocetku - ocistiti countere i mape
        emitLDCounter.clearAndSync();
        transNextLDCounter.clearAndSync();
        transLDCounter.clearAndSync();

        // Jesmo li dosli pred kraj? - je li prosli stupac bio zadnja bijela regija? Jesmo li sada na zadnjem stupcu?
        if((HMMIndex+1) >= this->conservedRegions.size()) {
            // Za zadnjega treba racunati D -> E, M -> E, D -> I, E -> I;
            // dodatno, treba racunati I -> I, I -> E
            // Znaci, sve vezano uz End
            this->endCalc(transLDCounter, transNextLDCounter, emitLDCounter, currCol);
            return;
        }

        // Prebrojati znakove u stupcu
        transLDCounter();

        /*
        Automatski smo u bijeloj zoni - zato sto na pocetku algoitma se preskoci siva zona.
        Kasnije u algoritmu, ukoliko poslije bijele zone slijedi siva, onda se ta siva zona isto preskoci,
        i algoritam nastavlja dalje sa bijelom zonom.

        Tako da zapravo jedino trebamo provjeravati je li slijedeca zona bijela ili siva. I naravno na pocetku
        algoritma, prilikom obrade S i I0 stanja, treba provjeriti je li 0. stupac bijela ili siva zona
        */
        // Bijela zona, racunamo vjerojatnosti za Dx i Mx, ali i za Ix
        // Trebamo izbrojati u sljedecem stupcu stavke
        nextColumnConservedRegion = (currCol+1) == this->conservedRegions[HMMIndex];

        double transitionToMimatchState, transitionToDeleteState, transitionToInsertionState;

        if(nextColumnConservedRegion) {
            // Iduca bijela -  B->B
            // M -> M ili M -> D
            // Broj M stanja - znaci letterCount
            // Ako ide u M, onda u sljedecem stupcu moramo gledati letterCount
            // A ako ide u D, onda u sljedecem stupcu moramo gledati dashCount
            (++transNextLDCounter)();

            denominatorTmp = (transLDCounter.letterCount + this->PSEUDOCOUNT_TRANSITION_DENOMINATOR);
            transitionToMimatchState = ((double)transNextLDCounter.letterCount + this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;
            transitionToDeleteState = ((double)transNextLDCounter.dashCount + this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;
            transitionToInsertionState = (this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;

            tIt = this->transitionsMap.emplace(State{HMMIndex, State::Type::M}, unordered_map<State, double, State::HashFunction>());
            tIt.first->second[{HMMIndex + 1, State::Type::M}] = transitionToMimatchState;
            tIt.first->second[{HMMIndex + 1, State::Type::D}] = transitionToDeleteState;
            tIt.first->second[{HMMIndex, State::Type::I}] = transitionToInsertionState;

            // D -> M ili D -> D
            denominatorTmp = (transLDCounter.dashCount + this->PSEUDOCOUNT_TRANSITION_DENOMINATOR);
            transitionToMimatchState = ((double)transNextLDCounter.letterCount + this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;
            transitionToDeleteState = ((double)transNextLDCounter.dashCount + this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;
            transitionToInsertionState = (this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;

            tIt = this->transitionsMap.emplace(State{HMMIndex, State::Type::D}, unordered_map<State, double, State::HashFunction>());
            tIt.first->second[{HMMIndex + 1, State::Type::M}] = transitionToMimatchState;
            tIt.first->second[{HMMIndex +1, State::Type::D}] = transitionToDeleteState;
            tIt.first->second[{HMMIndex, State::Type::I}] = transitionToInsertionState;

            // Emisije za Mimatch stanje
            denominatorTmp = transLDCounter.letterCount + this->PSEUDOCOUNT_EMISSION_DENOMINATOR;
            auto eIt = this->emissionsMap.emplace(State{HMMIndex, State::Type::M}, unordered_map<char, double>());
            for(const auto& currEntry : transLDCounter.letterFreqs) {
                char currSubChar = currEntry.first;
                int currSubCharNum = currEntry.second;

                eIt.first->second[currSubChar] = ((double)currSubCharNum + this->PSEUDOCOUNT_EMISSION)/denominatorTmp;
            }
        } else {
            // Iduca siva - B->S -- na kraju treba preskociti sivu zonu na indexu sekvence
            // M -> I(AC-C) ili negdje kasnije(unutar sive npr. --A) moze ici u M -> I, a tek u sljedecoj bijeloj ide M -> M i M -> D
            // M -> I, ali treba gledati i D -> I
            nextConsrvRegNum = this->conservedRegions[HMMIndex];
            for(int i = currCol; i < nextConsrvRegNum; i++) {
                ++transNextLDCounter;
            }
            transNextLDCounter();

            int mimatch2Insertion = 0, mimatch2Mimatch = 0, mimatch2Deletion = 0;
            int deletion2Insertion = 0, deletion2Mimatch = 0, deletion2Deletion = 0;
            int insertion2Insertion = 0, insertion2Mimatch  = 0, insertion2Deletion = 0;

            for(int currRow = 0; currRow < this->numOfRows; currRow++) {
                bool ideInsertion = false;
                auto& seqIt = this->MSATrackers[currRow];
                auto seqSubIt = seqIt;

                ++seqSubIt;
                for (int currSubCol = currCol + 1; currSubCol < nextConsrvRegNum; currSubCol++) {
                    currChar = *seqSubIt;
                    ++seqSubIt;

                    if(currChar != MyUtil::Algorithm::GAP) {
                        // Ako naide na bilo koji znak, znaci ide u Ix
                        ideInsertion = true;
                        emitLDCounter.addLetter(currChar);

                        // Moramo brojati Ins -> Ins
                        ++seqSubIt;
                        for(++currSubCol; currSubCol < nextConsrvRegNum; currSubCol++) {
                            currChar = *seqSubIt;
                            ++seqSubIt;
                            if(currChar != MyUtil::Algorithm::GAP) {
                                insertion2Insertion++;
                                emitLDCounter.addLetter(currChar);
                            }
                        }

                        break;
                    }
                }

                currChar = *seqIt;
                if(ideInsertion) {
                    // Iz M je otisao u I
                    if(currChar != MyUtil::Algorithm::GAP) {
                        mimatch2Insertion++;
                    } else {
                        deletion2Insertion++;
                    }

                    // Otisli smo u Ix - moramo gledati gdje cemo dalje iz Ix ici - gledamo sljedeci bijeli znak
                    currChar = *seqSubIt;
                    if(currChar != MyUtil::Algorithm::GAP) {
                        insertion2Mimatch++;
                    } else {
                        insertion2Deletion++;
                    }
                } else {
                    // Nije bilo insertiona, pa samo gledamo sta dolazi poslije sive regije
                    // M -> M, M -> D, te D -> M i D -> D
                    if(currChar != MyUtil::Algorithm::GAP) {
                        // Bili smo u M - sad gledamo sljedeci bijeli, tj. gdje cemo otici
                        currChar = *seqSubIt;
                        if(currChar != MyUtil::Algorithm::GAP) {
                            // M -> M
                            mimatch2Mimatch++;
                        } else {
                            // M -> D
                            mimatch2Deletion++;
                        }
                    } else {
                        // Bili smo u D - sad gledamo sljedeci bijeli, tj. gdje cemo otici
                        currChar = *seqSubIt;
                        if(currChar != MyUtil::Algorithm::GAP) {
                            // Otisli smo u M -- D -> M
                            deletion2Mimatch++;
                        } else {
                            // Otisli smo u D -- D -> D
                            deletion2Deletion++;
                        }
                    }
                }
            }

            // Poslije svih redaka mozemo konacno izracunati vjerojatnosti
            // M -> M, M -> D, i M -> I
            // Bili smo u Bijeloj regiji, broj M je letterCount u nazivniku
            denominatorTmp = transLDCounter.letterCount + this->PSEUDOCOUNT_TRANSITION_DENOMINATOR;
            transitionToDeleteState = ((double)mimatch2Deletion + this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;
            transitionToMimatchState = ((double)mimatch2Mimatch + this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;
            transitionToInsertionState = ((double)mimatch2Insertion + this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;

            tIt = this->transitionsMap.emplace(State{HMMIndex, State::Type::M}, unordered_map<State, double, State::HashFunction>());
            tIt.first->second[{HMMIndex + 1, State::Type::D}] = transitionToDeleteState;
            tIt.first->second[{HMMIndex + 1, State::Type::M}] = transitionToMimatchState;
            tIt.first->second[{HMMIndex, State::Type::I}] = transitionToInsertionState;

            // D -> M, D -> D i D -> I
            denominatorTmp = transLDCounter.dashCount + this->PSEUDOCOUNT_TRANSITION_DENOMINATOR;
            transitionToDeleteState = ((double)deletion2Deletion + this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;
            transitionToMimatchState = ((double)deletion2Mimatch + this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;
            transitionToInsertionState = ((double)deletion2Insertion + this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;

            tIt = this->transitionsMap.emplace(State{HMMIndex, State::Type::D}, unordered_map<State, double, State::HashFunction>());
            tIt.first->second[{HMMIndex + 1, State::Type::D}] = transitionToDeleteState;
            tIt.first->second[{HMMIndex + 1, State::Type::M}] = transitionToMimatchState;
            tIt.first->second[{HMMIndex, State::Type::I}] = transitionToInsertionState;

            // Ix -> Ix, Ix -> D_x+1, Ix -> I_x+1
            // denominatorTmp = zbroj(sveukupno) odlazaka iz Insertiona
            denominatorTmp = mimatch2Insertion + deletion2Insertion + insertion2Insertion + this->PSEUDOCOUNT_TRANSITION_DENOMINATOR;
            transitionToMimatchState = ((double)insertion2Mimatch + this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;
            transitionToDeleteState = ((double)insertion2Deletion + this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;
            transitionToInsertionState = ((double)insertion2Insertion + this->PSEUDOCOUNT_TRANSITION)/denominatorTmp;

            tIt = this->transitionsMap.emplace(State{HMMIndex, State::Type::I}, unordered_map<State, double, State::HashFunction>());
            tIt.first->second[{HMMIndex+1, State::Type::D}] = transitionToDeleteState;
            tIt.first->second[{HMMIndex+1, State::Type::M}] = transitionToMimatchState;
            tIt.first->second[{HMMIndex, State::Type::I}] = transitionToInsertionState;

            // Emisije za M stanje
            denominatorTmp = transLDCounter.letterCount + this->PSEUDOCOUNT_EMISSION_DENOMINATOR;
            auto eIt = this->emissionsMap.emplace(State{HMMIndex, State::Type::M}, unordered_map<char, double>());
            for(const auto& currEntry : transLDCounter.letterFreqs) {
                char currSubChar = currEntry.first;
                int currSubCharNum = currEntry.second;

                eIt.first->second[currSubChar] = ((double)currSubCharNum + this->PSEUDOCOUNT_EMISSION)/denominatorTmp;
            }

            // Vrijeme za transmisije u I stanju
            denominatorTmp = emitLDCounter.letterCount + this->PSEUDOCOUNT_EMISSION_DENOMINATOR;
            eIt = this->emissionsMap.emplace(State{HMMIndex, State::Type::I}, unordered_map<char, double>());
            for(const auto& currEntry : emitLDCounter.letterFreqs) {
                char currSubChar = currEntry.first;
                int currSubCharNum = currEntry.second;

                eIt.first->second[currSubChar] = ((double)currSubCharNum + this->PSEUDOCOUNT_EMISSION)/denominatorTmp;
            }

            // Preskociti sivu regiju, pomaknuti HMMindex
            // TODO: Kada god micemo currCol, moramo i pripadne iteratore pomaknuti da pokazuju na kraj sive regije
            for(int i = 0; i < this->MSATrackers.size(); i++) {
                for(int j = currCol; j < nextConsrvRegNum; j++) {
                    this->MSATrackers[i]++;
                }
            }
            currCol = nextConsrvRegNum - 1;
        }

        /*
			Posto preskacemo sive regije(te sukladno tome pomicemo i currCol index, to znaci da kada god dodemo ovdje
			smo obradili 1 bijelu zonu, dakle povecavamo HMMIndex koji defakto prati trenutan indeks DIM stanja,
			tj. trenutan indeks u HMM-u(za razliku od currCol koji prati trenutan indeks u sekvencama).
			 */
        HMMIndex++;
        // FIXME: A Mozda i tu treba pomaknuti iteratore? Mislim da ne, posto samo currCol je defakto povezan s iteratorima
    }
}

void ProfileHMM::endCalc(ProfileHMM::LDCounter &transLDCounter, ProfileHMM::LDCounter &transNextLDCounter,
                         ProfileHMM::LDCounter &emitLDCounter, int currCol) {
    int HMMIndex = this->conservedRegions.size();

    // Clear counters
    transLDCounter.clearAndSync();
    transNextLDCounter.clearAndSync();
    emitLDCounter.clearAndSync();

    // Sada mozemo krenuti racunati tu zadnju regiju - nalazimo se u sekvencama na zadnjoj bijeloj regiji
    transLDCounter();

    // M -> E, D -> E ; NO, moguce su i tranzicije M -> I, D -> I
    // Nadalje, tranzicije za I: I -> I, I -> E
    int mimatch2End = 0, mimatch2Insertion = 0, deletion2End = 0, deletion2Insertion = 0;
    int insertion2Insertion = 0, insertion2End = 0;
    char currChar;

    for(int currSubRow = 0; currSubRow < this->numOfRows; currSubRow++) {
        auto& seqIt = this->MSATrackers[currSubRow];
        auto subSeqIt = seqIt;

        for(int currSubCol = currCol+1; currSubCol < this->numOfCols; currSubCol++) {
            // Provjeriti hocemo li ici u Insertion?
            currChar = *(++subSeqIt);

            if(currChar != MyUtil::Algorithm::GAP) {
                // Otisli smo u Insertion - treba provjeriti jesmo li otisli u I iz M ili D
                // Potrebno pobrojati emitiran znak u Im stanju
                emitLDCounter.addLetter(currChar);

                currChar = *seqIt;
                if(currChar != MyUtil::Algorithm::GAP) {
                    mimatch2Insertion++;
                } else {
                    deletion2Insertion++;
                }

                // Potrebno je brojati koliko loopova se bude desilo prije Enda: Im -> Im
                for(++currSubCol; currSubCol < this->numOfCols; currSubCol++) {
                    currChar = *(++subSeqIt);

                    if(currChar != MyUtil::Algorithm::GAP) {
                        emitLDCounter.addLetter(currChar);
                        insertion2Insertion++;
                    }
                }

                // Izasli smo iz sive regije u nista, tj. na kraj
                break;
            }
        }
    }

    // Sada treba izracunati vjerojatnosti
    // Prvo Dm -> Im, i D -> E
    double denominatorTmp = 2 * this->PSEUDOCOUNT_TRANSITION;			// Mnozimo s 2 jer postoje 2 vjerojatnosti za 2 strelice
    double transition2Insertion, transition2End;

    transition2Insertion = ((double)deletion2Insertion + this->PSEUDOCOUNT_TRANSITION)/(transLDCounter.dashCount + denominatorTmp);
    transition2End = 1 - transition2Insertion;

    auto tIt = this->transitionsMap.emplace(State{HMMIndex, State::Type::D}, unordered_map<State, double, State::HashFunction>());
    tIt.first->second[{HMMIndex, State::Type::I}] = transition2Insertion;
    tIt.first->second[{HMMIndex, State::Type::E}] = transition2End;

    // Sada ide Mm -> Im, i Mm -> E
    transition2Insertion = ((double)mimatch2Insertion + this->PSEUDOCOUNT_TRANSITION)/(transLDCounter.letterCount + denominatorTmp);
    transition2End = 1 - transition2Insertion;

    tIt = this->transitionsMap.emplace(State{HMMIndex, State::Type::M}, unordered_map<State, double, State::HashFunction>());
    tIt.first->second[{HMMIndex, State::Type::I}] = transition2Insertion;
    tIt.first->second[{HMMIndex, State::Type::E}] = transition2End;

    // Sada treba izracunati vjerojatnosti za Im -> Im, i Im -> E
    double insertionUkupnoOut = mimatch2Insertion + deletion2Insertion + insertion2Insertion + denominatorTmp;
    transition2Insertion = ((double)insertion2Insertion + this->PSEUDOCOUNT_TRANSITION)/insertionUkupnoOut;
    transition2End = 1 - transition2Insertion;

    tIt = this->transitionsMap.emplace(State{HMMIndex, State::Type::I}, unordered_map<State, double, State::HashFunction>());
    tIt.first->second[{HMMIndex, State::Type::I}] = transition2Insertion;
    tIt.first->second[{HMMIndex, State::Type::E}] = transition2End;

    // Sanse emisije za M i I stanja
    // Prvo za M emisije
    auto eIt = this->emissionsMap.emplace(State{HMMIndex, State::Type::M}, unordered_map<char, double>());
    denominatorTmp = transLDCounter.letterCount + this->PSEUDOCOUNT_EMISSION_DENOMINATOR;
    for(const auto& currEntry : transLDCounter.letterFreqs) {
        char currSubChar = currEntry.second;
        int currSubCharNum = currEntry.first;

        eIt.first->second[currSubChar] = ((double)currSubCharNum + this->PSEUDOCOUNT_EMISSION)/denominatorTmp;
    }

    // Vrijeme za I emisije
    denominatorTmp = emitLDCounter.letterCount + this->PSEUDOCOUNT_EMISSION_DENOMINATOR;
    eIt = this->emissionsMap.emplace(State{HMMIndex, State::Type::I}, unordered_map<char, double>());
    for(const auto& currEntry : emitLDCounter.letterFreqs) {
        char currSubChar = currEntry.second;
        int currSubCharNum = currEntry.first;

        eIt.first->second[currSubChar] = ((double)currSubCharNum + this->PSEUDOCOUNT_EMISSION)/denominatorTmp;
    }
}