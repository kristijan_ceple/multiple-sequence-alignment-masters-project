//
// Created by kikyy_99 on 10.10.21.
//

#include "AlignmentAlgorithm.h"
#include <iostream>

AlignmentAlgorithm::AlignmentAlgorithm(std::vector<std::list<char>>& trainingSequences, std::vector<std::list<char>>& toAlignSequences)
    : trainingSequences{trainingSequences}, toAlignSequences{toAlignSequences} {
    if(trainingSequences.size() < 2 || toAlignSequences.size() < 2) {
        throw std::invalid_argument("Alignment Algorithm training sequences and to align sequences need to contain at least 2 sequences each!");
    }
}

std::vector<std::list<char>> &AlignmentAlgorithm::alignSequences() {
//    std::cout << "Algorithm Alignment()" << std::endl;
    this->aligned = true;
    return this->toAlignSequences;
}

AlignmentAlgorithm::~AlignmentAlgorithm() = default;
