//
// Created by kikyy_99 on 15. 01. 2022..
//

#ifndef ALS_VITERBI_H
#define ALS_VITERBI_H


#include "ProfileHMM.h"
#include "MyUtil.h"
#include "Array2DOwner.h"

class Viterbi {
    using HMMState = ProfileHMM::State;
    using HMMSType = HMMState::Type;
    friend class ProgressiveMSA_HMM_Alignment_Algorithm;

private:
    class ViterbiGraphCell {
    private:
        friend class Viterbi;
        enum class ViterbiGraphState: uint8_t {D = 0, I = 1, M = 2, S = 3, E = 4};
        inline friend std::ostream& operator<<(std::ostream& out, const ViterbiGraphState& y) {
            out << vgs2Char(y);
            return out;
        }

        ViterbiGraphState cellState = ViterbiGraphState::S;
        int stateIndex = -1;
        double cellVal = 0.;

        ViterbiGraphCell() = default;
        ViterbiGraphCell(ViterbiGraphState cellState, int stateIndex, double cellVal);

//        auto operator<=>(const ViterbiGraphCell& other) const = default;

        inline friend std::ostream& operator<<(std::ostream& os, const ViterbiGraphCell& vgc) {
            os << vgs2Char(vgc.cellState) << vgc.stateIndex;
            return os;
        };

        // Move assignment operator
        ViterbiGraphCell& operator=(ViterbiGraphCell&& other) noexcept =default;

        constexpr static inline char vgs2Char(ViterbiGraphState in) {
            switch(in) {
                case ViterbiGraphState::D: return 'D';
                case ViterbiGraphState::I: return 'I';
                case ViterbiGraphState::M: return 'M';
                case ViterbiGraphState::S: return 'S';
                case ViterbiGraphState::E: return 'E';
                default: return 0;
            }
        }

        constexpr static inline ProfileHMM::State::Type vgs2HMMStateType(ViterbiGraphState in) {
            switch(in) {
                case ViterbiGraphState::D: return HMMSType::D;
                case ViterbiGraphState::I: return HMMSType::I;
                case ViterbiGraphState::M: return HMMSType::M;
                case ViterbiGraphState::S: return HMMSType::S;
                case ViterbiGraphState::E: return HMMSType::E;
                default: return HMMSType::E;
            }
        }

        static inline ProfileHMM::State vgc2HMMState(const ViterbiGraphCell& in) {
            return ProfileHMM::State{in.stateIndex, vgs2HMMStateType(in.cellState)};
        }

        class HashFunction {
        public:
            size_t operator()(const ViterbiGraphCell& vgc) const {
                size_t seed = 0;
                boost::hash_combine(seed, vgc.cellState);
                boost::hash_combine(seed, vgc.stateIndex);
                boost::hash_combine(seed, vgc.cellVal);
                return seed;
            }
        };
    };

    using VGState = Viterbi::ViterbiGraphCell::ViterbiGraphState;

    const ProfileHMM* profileHMM = nullptr;
    Array2DOwner<ViterbiGraphCell> VGTable{};
    std::list<char>* sequenceToAlign = nullptr;

    std::unordered_map<ProfileHMM::State, std::unordered_map<ProfileHMM::State::Type, double>, ProfileHMM::State::HashFunction> logTransitionProbs;
    std::unordered_map<ProfileHMM::State, std::unordered_map<char, double>, ProfileHMM::State::HashFunction> logEmissionProbs;

    void viterbiGraph();
    void traceback(std::vector<ViterbiGraphCell::ViterbiGraphState>& viterbiPath);
public:
    Viterbi() = delete;
    explicit Viterbi(const ProfileHMM& profileHMM);
    void fillLogMaps();
    std::vector<ViterbiGraphCell::ViterbiGraphState> getViterbiPath(std::list<char>& sequence);
};


#endif //ALS_VITERBI_H
