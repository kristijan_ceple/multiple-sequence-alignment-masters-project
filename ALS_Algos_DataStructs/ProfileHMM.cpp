//
// Created by kikyy_99 on 06.10.21.
//

#include "ProfileHMM.h"
#include "MyUtil.h"
#include <unordered_map>
#include <list>
#include <execution>
using namespace std;

ProfileHMM::ProfileHMM(const std::vector<std::list<char>>& MSA, int numberOfSeqsToBuildHMMFor,
                       double PSEUDOCOUNT_EMISSION, double PSEUDOCOUNT_TRANSITION, double COLUMN_REMOVAL_THRESHOLD)
                       : PSEUDOCOUNT_EMISSION{PSEUDOCOUNT_EMISSION}, PSEUDOCOUNT_TRANSITION{PSEUDOCOUNT_TRANSITION},
                         COLUMN_REMOVAL_THRESHOLD{COLUMN_REMOVAL_THRESHOLD}, id{ProfileHMM::idCounter++}, MSAFull{MSA}
{
    /*
     * First move references to the sequences which will be used to build the Profile HMM into a special
     * container
     */
    for(int i = 0; i < numberOfSeqsToBuildHMMFor; i++) {
        this->MSAForHMMBuilding.emplace_back(MSA[i]);
    }

    // TODO: Remove columns with all '-'-s here?
    this->numOfRows = this->MSAForHMMBuilding.size();
    this->numOfCols = this->MSAForHMMBuilding.front().get().size();

    for(int i = 0; i < this->MSAForHMMBuilding.size(); i++) {
        this->MSATrackers.emplace_back(this->MSAForHMMBuilding[i].get().cbegin());
    }

    this->initParameters();
}

void ProfileHMM::initParameters() {
    this->makeCharsSet();
    this->filterConservedRegions();
    this->PSEUDOCOUNT_EMISSION_DENOMINATOR = (this->charactersSet.size()-1) * this->PSEUDOCOUNT_EMISSION;                   // Size - 1 because we do not cout the '-'s
    this->PSEUDOCOUNT_TRANSITION_DENOMINATOR = ProfileHMM::HMM_OUTGOING_TRANSITIONS * this->PSEUDOCOUNT_TRANSITION;         // TODO: Check if maybe wrong pseudocount variable was used here???
    this->calculateProbabilities();
}

void ProfileHMM::makeCharsSet() {
    for(auto const& seqWrapper : this->MSAForHMMBuilding) {
        auto const& seq = seqWrapper.get();
        for(int i = 0; i < seq.size(); i++) {
            for(char currChar : seq) {
                this->charactersSet.emplace(currChar);
            }
        }
    }

    for(char currChar : this->charactersSet) {
        if(currChar != MyUtil::Algorithm::GAP) {
            this->charsFreqBlueprint.emplace(currChar, 0);
        }
    }
}

void ProfileHMM::filterConservedRegions() {
    vector<list<char>::const_iterator> currentColumn;
    currentColumn.reserve(this->numOfRows);
    for(const auto& seqWrapper : this->MSAForHMMBuilding) {
        auto const& seq = seqWrapper.get();
        currentColumn.emplace_back(seq.cbegin());
    }

    /*
     * Count characters column by column
     * Accordingly, biologists often ignore columns for which the fraction of space
     * symbols is greater than or equal to a column removal threshold q
     */
    unsigned int dashCounter;
    for(int j = 0; j < this->numOfCols; j++) {
        dashCounter = 0;
        for(auto& seqIt : currentColumn) {
            // Count it in the map
            if(*seqIt == '-') {
                ++dashCounter;
            }

            ++seqIt;    // Move to the next char/column
        }

        // Let's check if this column is a conserved region
        if(((double)dashCounter/this->numOfRows) < this->COLUMN_REMOVAL_THRESHOLD) {
            this->conservedRegions.emplace_back(j);
        }
    }
}

void ProfileHMM::calculateProbabilities() {
    // This is where the fun begins
    int HMMIndex = 1;
    double denominatorTmp;

    // TODO: Implement cloning here?
    LDCounter transLDCounter{*this};
    LDCounter transNextLDCounter{*this};
    LDCounter emitLDCounter{*this};

    /*
    Pripremiti sva moguca stanja - de fakto HMM = mapa tranzicijskih i emisijskih vjerojatnosti
    m = broj konzerviranih regija
    Start stanje, End stanje
    m Deletion stanja(D1 do Dm)
    m Mimatch stanja(M1 do Mm)
    m+1 Insertion stanja(I0 do Im)
    */

    // Popuniti mapu za emisije
    size_t m = this->conservedRegions.size();

    // ##############################				INIT				############################################
    charactersSet.erase(MyUtil::Algorithm::GAP);

    // Emissions
    // Prvo Mimatch stanja(ima ih m)
    for(int i = 1; i <= m; i++) {
        auto it = this->emissionsMap.emplace(State{i, State::Type::M}, unordered_map<char, double>());
        for(char currChar : this->charactersSet) {
            it.first->second[currChar] = 0.;
        }
    }

    // Emissions
    // Zatim Insertion stanja(ima ih m+1, tj. indeks im krece od 0)
    for(int i = 0; i <= m; i++) {
        auto it = this->emissionsMap.emplace(State{i, State::Type::I}, unordered_map<char, double>());
        for(char currChar : this->charactersSet) {
            it.first->second[currChar] = 0.;
        }
    }

    charactersSet.emplace(MyUtil::Algorithm::GAP);

    // Transitions
    // Start i I0
    auto tIt = this->transitionsMap.emplace(State{0, State::Type::S}, unordered_map<State::Type, double>());
    tIt.first->second[State::Type::M] = 0.;
    tIt.first->second[State::Type::D] = 0.;
    tIt.first->second[State::Type::I] = 0.;

    // I0 sada
    this->transitionsMap.emplace(State{0, State::Type::I}, tIt.first->second);

    for(int i = 1; i < m; i++) {
        // Di -> D_i+1, Di -> Ii, Di -> M_i+1
        tIt = this->transitionsMap.emplace(State{i, State::Type::D}, unordered_map<State::Type, double>());
        tIt.first->second[State::Type::D] = 0.;
        tIt.first->second[State::Type::I] = 0.;
        tIt.first->second[State::Type::M] = 0.;

        // Mi -> M_i+1, Mi -> Ii, Mi -> D_i+1
        this->transitionsMap.emplace(State{i, State::Type::M}, tIt.first->second);

        // Ii -> Ii, Ii -> D_i+1, Ii -> M_i+1
        this->transitionsMap.emplace(State{i, State::Type::I}, tIt.first->second);
    }

    // End i Im - handliranje zadnjeg retka
    // Dm -> E, Dm -> Im ; Mm -> E, Mm -> Im ; Im -> Im, Im -> E
    tIt = this->transitionsMap.emplace(State{static_cast<int>(m), State::Type::D}, unordered_map<State::Type, double>());
    tIt.first->second[State::Type::E] = 0.;
    tIt.first->second[State::Type::I] = 0.;

    this->transitionsMap.emplace(State{static_cast<int>(m), State::Type::M}, tIt.first->second);

    this->transitionsMap.emplace(State{static_cast<int>(m), State::Type::I}, tIt.first->second);
    // ##############################				INIT				############################################

    // ##############################			GLAVNI DIO - PROBS CALC			####################################
    /*
     * 1. Build all paths
     * 2. Go over them, count the transitions and emissions and put them into maps
     * 3. Finally, turn the absolute numbers(counts) in the map into relative frequency probabilities. Add pseudocounts
     */

    // 1. Build all paths
    vector<vector<State::Type>> HMMPaths{};
    HMMPaths.reserve(this->numOfRows);

    // Do this to prevent overflow, but remove it later!
    this->conservedRegions.emplace_back(this->numOfCols);

    for(int i = 0; i < this->numOfRows; i++) {
        const auto& currSeq = this->MSAForHMMBuilding[i].get();
        auto& currPathBuilder = HMMPaths.emplace_back();
        currPathBuilder.reserve(currSeq.size()+2);          // +2 to account for S and E states

        currPathBuilder.emplace_back(State::Type::S);

        int currConservRegNum, currConservRegIndex = 0;
        if(this->conservedRegions.size() == 1) {
            // No conserved regions, means we will only have insertions
            int lettersCount = std::count_if(
                    std::execution::par_unseq,
                    currSeq.cbegin(),
                    currSeq.cend(),
                    [](char in) { return (in != MyUtil::Algorithm::GAP); }
                    );
            std::fill_n(
                    std::execution::par_unseq,
                    currPathBuilder.begin(),
                    lettersCount,
                    State::Type::I
                    );
            currPathBuilder.emplace_back(State::Type::E);
            continue;
        }

        // Else we can go on as normal
        auto seqIter = currSeq.cbegin();
        currConservRegNum = this->conservedRegions[0];
        for(int j = 0; j < currSeq.size(); j++) {
            bool currIndexConserved = (j == currConservRegNum);
            char currChar = *(seqIter++);

            if(currIndexConserved) {
                currPathBuilder.emplace_back(currChar != MyUtil::Algorithm::GAP ? State::Type::M : State::Type::D);
                currConservRegNum = this->conservedRegions[++currConservRegIndex];
            } else {
                if(currChar != MyUtil::Algorithm::GAP) {
                    currPathBuilder.emplace_back(State::Type::I);
                }
            }
        }

        currPathBuilder.emplace_back(State::Type::E);
    }

    // 2. Counting
    int tmpHMMIndex;
    char tmpChar;
//    int currConservRegNum = this->conservedRegions[0], currConservRegIndex = -1;
    for(int k = 0; k < HMMPaths.size(); k++) {
        const auto& currPath = HMMPaths[k];
        const auto& currSeq = this->MSAForHMMBuilding[k].get();

        auto currSeqIt = currSeq.cbegin();
        int currHMMIndex = 0, currConservRegNum, currSeqIndex = -1;
        State::Type currType = State::Type::S, nextType = currPath[1];

        // First Handle the S state
        auto& mapS0 = this->transitionsMap[State{0, State::Type::S}];
        ++mapS0[nextType];

        // Then the rest
        for(int i = 2; i < currPath.size(); i++) {
            ++currSeqIndex;

            currType = currPath[i - 1];
            nextType = currPath[i];

            if(currType != State::Type::I) {
                ++currHMMIndex;
            }
            tmpHMMIndex = (currType != State::Type::I) ? currHMMIndex : currHMMIndex-1;

            auto& tmpTransMap = this->transitionsMap[State{tmpHMMIndex, currType}];
            ++tmpTransMap[nextType];

            // Advance sequence letters
            tmpChar = *(currSeqIt++);
            ++currSeqIndex;

            if(currType == State::Type::I && nextType != State::Type::I) {
                // Advance currSeqIt to next conserved region
                currConservRegNum = this->conservedRegions[currHMMIndex - 1];
                while(currSeqIndex < currConservRegNum) {
                    ++currSeqIt;
                    ++currSeqIndex;
                }
            }

            if(currType != State::Type::D) {
                // Handle emissions as well
                auto& tmpEmitMap = this->emissionsMap[State{tmpHMMIndex, currType}];
                ++tmpEmitMap[tmpChar];
            }
        }
    }

    // Remove the unnecessary last element
    this->conservedRegions.pop_back();

    // 3. Relative frequencies(+ pseudocounts)
    // Let's first process the emissions since that seems easier
    double denominator;
    int presentVals;
    for(auto& [key, valMap] : this->emissionsMap) {
        presentVals = 0;
        std::for_each(
                valMap.cbegin(),
                valMap.cend(),
                [&presentVals](const auto& in) {presentVals += in.second;}
                );

        denominator = presentVals + this->PSEUDOCOUNT_EMISSION * (this->charactersSet.size() - 1);
        for(auto& [subKey, subVal] : valMap) {
            valMap[subKey] = (subVal + this->PSEUDOCOUNT_EMISSION) / (denominator);
        }
    }

    // Now on to transitions -> Watch out for edge cases(End state and empty conserved regions set)
    if(!this->conservedRegions.empty()) {
        for(auto& [key, valMap] : this->transitionsMap) {
            presentVals = 0;
            std::for_each(
                    valMap.cbegin(),
                    valMap.cend(),
                    [&presentVals](const auto& in) {presentVals += in.second;}
            );

            if(key == State{static_cast<int>(m), State::Type::I} || key == State{static_cast<int>(m), State::Type::M} || key == State{static_cast<int>(m), State::Type::D}) {
                // End state cases
                denominator = presentVals + this->PSEUDOCOUNT_TRANSITION * (HMM_OUTGOING_TRANSITIONS - 1);
                for(auto& [subKey, subVal] : valMap) {
                    valMap[subKey] = (subVal + this->PSEUDOCOUNT_TRANSITION) / (denominator);
                }
            } else {
                // Normal cases
                denominator = presentVals + this->PSEUDOCOUNT_TRANSITION * HMM_OUTGOING_TRANSITIONS;
                for(auto& [subKey, subVal] : valMap) {
                    valMap[subKey] = (subVal + this->PSEUDOCOUNT_TRANSITION) / (denominator);
                }
            }
        }
    } else {
        // S -> I0 -> E
        // First handle S state
        auto& valMap = this->transitionsMap[State{0, State::Type::S}];
        auto& [subKey, subVal] = *valMap.begin();
        valMap[subKey] = 1.0;

        // Now handle I state
        valMap = this->transitionsMap[State{0, State::Type::I}];
        presentVals = 0;
        std::for_each(
                valMap.cbegin(),
                valMap.cend(),
                [&presentVals](const auto& in) {presentVals += in.second;}
        );

        denominator = presentVals + this->PSEUDOCOUNT_TRANSITION * 2;
        for(auto& [subKey, subVal] : valMap) {
            valMap[subKey] = (subVal + this->PSEUDOCOUNT_TRANSITION) / (denominator);
        }

        // No Need to handle End state
    }
}

void ProfileHMM::LDCounter::clearAndSync() {
    this->letterCount = 0;
    this->dashCount = 0;
    this->letterFreqs.clear();
    this->columnTrackers = this->HMM.MSATrackers;
}

void ProfileHMM::LDCounter::addLetter(char toAdd) {
    this->letterCount++;
    auto [iterator, inserted] = this->letterFreqs.try_emplace(toAdd, 1);
    if(!inserted) {
        iterator->second++;
    }
}

void ProfileHMM::LDCounter::operator()() {
    for(int i = 0; i < this->columnTrackers.size(); i++) {
        char currChar = *this->columnTrackers[i];
        if(currChar != MyUtil::Algorithm::GAP) {
            this->letterCount++;
            auto [iterator, inserted] = this->letterFreqs.try_emplace(currChar, 1);
            if(!inserted) {
                iterator->second++;
            }
        } else {
            this->dashCount++;
        }
    }
}

ProfileHMM::LDCounter& ProfileHMM::LDCounter::operator++() {
    for(int i = 0; i < this->columnTrackers.size(); i++) {
        this->columnTrackers[i]++;
    }

    return *this;
}

ProfileHMM::LDCounter& ProfileHMM::LDCounter::operator--() {
    for(int i = 0; i < this->columnTrackers.size(); i++) {
        this->columnTrackers[i]--;
    }

    return *this;
}

ProfileHMM::LDCounter::LDCounter(const ProfileHMM &HMM)  : HMM{HMM} {
//    for(int i = 0; i < HMM.MSA.size(); i++) {
//        this->columnTrackers.emplace_back(HMM.MSA[i].cbegin());
//    }
    this->columnTrackers = HMM.MSATrackers;
}

//const ProfileHMM::LDCounter ProfileHMM::LDCounter::operator++(int) {
//    LDCounter old = *this;
//    this->operator++();
//    return old;
//}
//
//const ProfileHMM::LDCounter ProfileHMM::LDCounter::operator--(int) {
//    LDCounter old = *this;
//    this->operator--();
//    return old;
//}

inline std::ostream& operator<<(std::ostream& os, ProfileHMM::State::Type inState) {
    os << ProfileHMM::ProfileHMMStateType2Char(inState);
    return os;
}

inline std::ostream &operator<<(ostream &os, const ProfileHMM::State& inState) {
    os << inState.type << inState.index;
    return os;
}

inline char ProfileHMM::ProfileHMMStateType2Char(ProfileHMM::State::Type in) {
    switch(in) {
        case ProfileHMM::State::Type::I:
            return 'I';
        case ProfileHMM::State::Type::M:
            return 'M';
        case ProfileHMM::State::Type::D:
            return 'D';
        case ProfileHMM::State::Type::S:
            return 'S';
        case ProfileHMM::State::Type::E:
            return 'E';
        default:
            return 'X';
    }
}

std::ostream &operator<<(ostream &os, const ProfileHMM &phmm) {
    os << "### Profile HMM " << phmm.id << " ###" << endl;
    os << "Emissions map:" << endl;

    for(const auto& [key, val] : phmm.emissionsMap) {
        os << "\t" << key << ":" << endl;
        for(const auto& [subKey, subVal] : val) {
            os << "\t\t" << subKey << ": " << subVal*100 << "%" << endl;
        }
    }

    os << "Transitions map:" << endl;
    for(const auto& [key, val] : phmm.transitionsMap) {
        os << "\t" << key << ":" << endl;
        for(const auto& [subKey, subVal] : val) {
            os << "\t\t" << subKey << ": " << subVal*100 << "%" << endl;
        }
    }

    return os;
}

void ProfileHMM::addLastConservedRegion() {
    this->conservedRegions.emplace_back(this->MSAForHMMBuilding[0].get().size());
}

void ProfileHMM::removeLastConservedRegion() {
    this->conservedRegions.pop_back();
}