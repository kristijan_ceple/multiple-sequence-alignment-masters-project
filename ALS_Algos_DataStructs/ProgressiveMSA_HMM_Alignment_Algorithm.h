//
// Created by kikyy_99 on 10.10.21.
//

#ifndef MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_PROGRESSIVEMSA_HMM_ALIGNMENT_ALGORITHM_H
#define MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_PROGRESSIVEMSA_HMM_ALIGNMENT_ALGORITHM_H

#include <vector>
#include <string>
#include "AlignmentAlgorithm.h"
#include "Viterbi.h"

class ProgressiveMSA_HMM_Alignment_Algorithm : public AlignmentAlgorithm {
    friend class ALS_GUI;

private:
    void assimilateSeqIntoMSA(const std::vector<Viterbi::VGState>& viterbiPath, int seqToAssimilateIndex, ProfileHMM& profileHMM);

protected:
    /**
     * The number of assimilated sequences
     */
    uint64_t AssimilatedMSALength = 0;

    double emissionPseudocount = 0, transitionPseudocount = 0, columnRemovalThreshold = 0;
public:
    ProgressiveMSA_HMM_Alignment_Algorithm() =delete;
    explicit ProgressiveMSA_HMM_Alignment_Algorithm(std::vector<std::list<char>>& trainingSequences, std::vector<std::list<char>>& toAlignSequences,
                                                    double emissionPseudocount = 0, double transitionPseudocount = 0, double columnRemovalThreshold = 0);
    std::vector<std::list<char>>& alignSequences() override;
};


#endif //MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_PROGRESSIVEMSA_HMM_ALIGNMENT_ALGORITHM_H
