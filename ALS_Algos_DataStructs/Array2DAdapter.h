//
// Created by kikyy_99 on 21.10.21.
//

#ifndef MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_ARRAY2DADAPTER_H
#define MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_ARRAY2DADAPTER_H


#include <stdexcept>

template<typename T>
class Array2DOwner;

template<typename T>
class Array2DAdapter {
private:
    T* array = nullptr;
    size_t cols = 0;

    friend class Array2DOwner<T>;
    Array2DAdapter() =default;
public:
    Array2DAdapter(T* array, size_t cols) : array{array}, cols{cols} {
        if(array == nullptr) {
            throw std::logic_error("Array was null!?");
        }
    }
    inline T& at(size_t i, size_t j);
    inline const T& at(size_t i, size_t j) const;

    // Move assignment
    Array2DAdapter<T>& operator=(Array2DAdapter<T>&& other) noexcept;
};

template<typename T>
inline T& Array2DAdapter<T>::at(size_t i, size_t j) {
    // Removing the code below for performance reasons
//     if(i < 0 || i >= this->rows || j < 0 || j >= this->columns) {
//        throw std::invalid_argument("Index out of bounds!");
//    }
    return array[this->cols * i + j];
}

template<typename T>
const T &Array2DAdapter<T>::at(size_t i, size_t j) const {
    return array[this->cols * i + j];
}

template<typename T>
Array2DAdapter<T> &Array2DAdapter<T>::operator=(Array2DAdapter<T> &&other) noexcept {
    // Guard self assignment
    if(this == &other) {
        return *this;
    }

    this->array = std::exchange(other.array, nullptr);
    this->cols = std::exchange(other.cols, 0);
    return *this;
}


#endif //MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_ARRAY2DADAPTER_H
