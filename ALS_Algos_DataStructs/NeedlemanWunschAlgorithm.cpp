//
// Created by kikyy_99 on 08.10.21.
//

#include <stdexcept>
#include <functional>
#include "NeedlemanWunschAlgorithm.h"
#include <iostream>

#define END_STATE_HANDLING

using namespace std;
using namespace MyUtil::Algorithm::NeedlemanWunsch;

NeedlemanWunschAlgorithm::NeedlemanWunschAlgorithm(std::vector<std::list<char>>& trainingSequences, std::vector<std::list<char>>& toAlignSequences)
    : AlignmentAlgorithm(trainingSequences, toAlignSequences) {
//    size_t seqN = trainingSequences[0].size();
//    for(int i = 1; i < seqN; i++) {
//        size_t subSeqN = trainingSequences[i].size();
//        if(subSeqN != seqN) {
//            throw invalid_argument("Training sequences need to be already aligned and of the same length!!!");
//        }
//    }
//    cout << "Training sequences are of the same size, and set. Initiating parameters of these aligned sequences..." << endl;

//    if(toAlignSequences.size() != 2) {
//        throw invalid_argument("Needleman-Wunsch accepts exactly two sequences to align as input!");
//    }

    this->initParameters();
    cout << "Parameters successfully initialized!" << endl;
}

void NeedlemanWunschAlgorithm::initParameters() {
    this->fillEmissionsMatrix();
    this->fillTransitionsMatrix();

    this->calculateEmissionProbabilities();
    this->calculateTransitionProbabilities();

    cout << "Emission probabilities matrix:" << endl;
    cout << emissionsMatrixToString<double>(&this->emissionProbabilitiesMatrix[0][0]) << endl;

    cout << "Transition probabilities matrix:" << endl;
    cout << transitionsMatrixToString<double>(&this->transitionProbabilitiesMatrix[0][0]) << endl;

    MyUtil::Algorithm::logOddsSquareMatrix(&this->emissionProbabilitiesMatrix[0][0], NW::EMISSIONS_MATRIX_CONST);
    MyUtil::Algorithm::logOddsSquareMatrix(&this->transitionProbabilitiesMatrix[0][0], NW::TRANSITIONS_MATRIX_CONST);
}

std::vector<std::list<char>> &NeedlemanWunschAlgorithm::alignSequences() {
//    if(this->toAlignSequences.size() != 2) {
//        throw invalid_argument("Needleman-Wunsch accepts exactly two sequences to align as input!");
//    }

    cout << "Running: Needleman-Wunsch pairwise alignment..." << endl;

    cout << "Removing redundant characters from the pair of sequences..." << endl;
    // Remove non 'A', 'C', 'G', 'T' in the sequences to be aligned
    auto lambda =[](char in) { return (in != 'A' && in != 'C' && in != 'G' && in != 'T'); };
    this->toAlignSequences[NW::PAIR_UPPER_SEQ_INDEX].remove_if(lambda);
    this->toAlignSequences[NW::PAIR_LOWER_SEQ_INDEX].remove_if(lambda);

    cout << "Forming the table and filling it in..." << endl;
    // Form the table first
    size_t ncol = this->toAlignSequences[NW::PAIR_UPPER_SEQ_INDEX].size();
    size_t nrow = this->toAlignSequences[NW::PAIR_LOWER_SEQ_INDEX].size();

    // Need to create this on heap!
//    double NWTable->at(nrow+1, ncol+1);
    Array2DOwner<double> NWTable((double*) new double[(nrow+1) * (ncol+1)], nrow+1, ncol+1);

    auto upperSeqIt = this->toAlignSequences[NW::PAIR_UPPER_SEQ_INDEX].cbegin();
    auto lowerSeqIt = this->toAlignSequences[NW::PAIR_LOWER_SEQ_INDEX].cbegin();

    /*
     * The algorithm calculation part:
     *
     * We start in the Begin state, and are located in the upper leftmost cell. From there, we calculate
     * first the 0th row and the 0th column, and then move on to the others
     */

    NWTable->at(0, 0) = 0.;
    double prevVal;

    // #################################################################################################################
    // FILLING THE FIRST ROW    ########################################################################################
    // #################################################################################################################
    auto tmpUpperSeqIt = this->toAlignSequences[NW::PAIR_UPPER_SEQ_INDEX].cbegin();

    // B -> Y
    prevVal = this->transitionProbabilitiesMatrix[static_cast<uint8_t>(TransitionState::B)][static_cast<uint8_t>(TransitionState::Y)]
            + this->emissionProbabilitiesMatrix[static_cast<uint8_t>(NucleotideBase::GAP)][NW::nucleotideBase2Index(
            *(tmpUpperSeqIt++))];
    NWTable->at(0, 1) = prevVal;

    // Y -> L --> Preparing for consecutive gaps
    prevVal = this->transitionProbabilitiesMatrix[static_cast<uint8_t>(TransitionState::Y)][static_cast<uint8_t>(TransitionState::L)]
                    + this->emissionProbabilitiesMatrix[static_cast<uint8_t>(NucleotideBase::GAP)][NW::nucleotideBase2Index(
            *(tmpUpperSeqIt++))]
                    + prevVal;
    NWTable->at(0, 2) = prevVal;

    // Now we're in state L, and the next state is L as well --> CONSECUTIVE GAPS
    for(int j = 3; j <= ncol; j++) {
        // The gaps in the lower sequence(transitions L -> L)
        prevVal = this->transitionProbabilitiesMatrix[static_cast<uint8_t>(TransitionState::L)][static_cast<uint8_t>(TransitionState::L)]
                          + this->emissionProbabilitiesMatrix[static_cast<uint8_t>(NucleotideBase::GAP)][NW::nucleotideBase2Index(
                *(tmpUpperSeqIt++))]
                          + prevVal;
        NWTable->at(0, j) = prevVal;
    }

    // #################################################################################################################
    // FILLING THE FIRST COLUMN    #####################################################################################
    // #################################################################################################################
    auto tmpLowerSeqIt = this->toAlignSequences[NW::PAIR_LOWER_SEQ_INDEX].cbegin();

    // B -> X
    prevVal = this->transitionProbabilitiesMatrix[static_cast<uint8_t>(TransitionState::B)][static_cast<uint8_t>(TransitionState::X)]
              + this->emissionProbabilitiesMatrix[NW::nucleotideBase2Index(*(tmpLowerSeqIt++))][static_cast<uint8_t>(NucleotideBase::GAP)];
    NWTable->at(1, 0) = prevVal;

    // X -> U --> preparing for consecutive gaps
    prevVal = this->transitionProbabilitiesMatrix[static_cast<uint8_t>(TransitionState::X)][static_cast<uint8_t>(TransitionState::U)]
              + this->emissionProbabilitiesMatrix[NW::nucleotideBase2Index(*(tmpLowerSeqIt++))][static_cast<uint8_t>(NucleotideBase::GAP)]
              + prevVal;
    NWTable->at(2, 0) = prevVal;

    // U -> U: Consecutive gaps!
    for(int i = 3; i <= nrow; i++) {
        prevVal = this->transitionProbabilitiesMatrix[static_cast<uint8_t>(TransitionState::U)][static_cast<uint8_t>(TransitionState::U)]
                  + this->emissionProbabilitiesMatrix[NW::nucleotideBase2Index(*(tmpLowerSeqIt++))][static_cast<uint8_t>(NucleotideBase::GAP)]
                  + prevVal;
        NWTable->at(i, 0) = prevVal;
    }

    // #################################################################################################################
    // Going row by row and filling the table   ########################################################################
    // #################################################################################################################
    TransitionState currStateLeft, nextStateLeft;
    TransitionState currStateTop, nextStateTop;
    TransitionState currStateDiag, nextStateDiag;

    // Calculate 3 values, and put the max of them into the table
    double fromLeft, fromTop, fromDiag;
    int lookAtCell_i, lookAtCell_j;
//    TransitionState statesTable[nrow][ncol];
    this->statesTable2D = Array2DOwner<TransitionState>(new TransitionState[nrow*ncol], nrow, ncol);

    char currLowerSeqChar, currUpperSeqChar;
    for(int i = 1; i <= nrow; i++) {
        /*
         *  Every time we move to the beginning of the row, we have to reset the upper sequence iterator to
         *  point to the beginning of the upper sequence
         */
        upperSeqIt = this->toAlignSequences[NW::PAIR_UPPER_SEQ_INDEX].cbegin();
        currLowerSeqChar = *(lowerSeqIt++);
        for(int j = 1; j <= ncol; j++) {
            /*
             * No need to reset the lower sequence, it just keeps moving down
             */
            currUpperSeqChar = *(upperSeqIt++);

            // Calculate fromLeft   ####################################################################################
            lookAtCell_i = i;
            lookAtCell_j = j-1;

            if(lookAtCell_j == 0 && lookAtCell_i > 1) {
                // 0th column
                currStateLeft = TransitionState::U;
            } else if(lookAtCell_j == 0 && lookAtCell_i == 1) {
                // lookAtCell_i = 1
                // 0th column, 1st row
                currStateLeft = TransitionState::X;
            } else {
                currStateLeft = this->statesTable2D->at(lookAtCell_i - 1, lookAtCell_j - 1);
            }
            nextStateLeft = (currStateLeft == TransitionState::Y || currStateLeft == TransitionState::L) ? TransitionState::L : TransitionState::Y;

            fromLeft = this->transitionProbabilitiesMatrix[static_cast<uint8_t>(currStateLeft)][static_cast<uint8_t>(nextStateLeft)]
                    + this->emissionProbabilitiesMatrix[NW::nucleotideBase2Index(currUpperSeqChar)][static_cast<uint8_t>(NucleotideBase::GAP)]
                    + NWTable->at(lookAtCell_i, lookAtCell_j);

#ifdef END_STATE_HANDLING
            // End state handling
            if(i == nrow && j == ncol) {
                fromLeft += this->transitionProbabilitiesMatrix[static_cast<uint8_t>(nextStateLeft)][static_cast<uint8_t>(TransitionState::E)];
            }
#endif

            // Calculate fromTop    ####################################################################################
            lookAtCell_i = i-1;
            lookAtCell_j = j;

            if(lookAtCell_i == 0 && lookAtCell_j > 1) {
                // 0th row
                currStateTop = TransitionState::L;
            } else if(lookAtCell_i == 0 && lookAtCell_j == 1) {
                // 0th row, 1st column
                currStateTop = TransitionState::Y;
            } else {
                currStateTop = this->statesTable2D->at(lookAtCell_i - 1, lookAtCell_j - 1);
            }
            nextStateTop = (currStateTop == TransitionState::X || currStateTop == TransitionState::U) ? TransitionState::U : TransitionState::X;

            fromTop = this->transitionProbabilitiesMatrix[static_cast<uint8_t>(currStateTop)][static_cast<uint8_t>(nextStateTop)]
                + this->emissionProbabilitiesMatrix[static_cast<uint8_t>(NucleotideBase::GAP)][NW::nucleotideBase2Index(
                    currLowerSeqChar)]
                + NWTable->at(lookAtCell_i, lookAtCell_j);

#ifdef END_STATE_HANDLING
            // End state handling
            if(i == nrow && j == ncol) {
                fromTop += this->transitionProbabilitiesMatrix[static_cast<uint8_t>(nextStateTop)][static_cast<uint8_t>(TransitionState::E)];
            }
#endif

            // Calculate fromDiag   ####################################################################################
            lookAtCell_i = i-1;
            lookAtCell_j = j-1;

            if(lookAtCell_i == 0 && lookAtCell_j > 1) {
                currStateDiag = TransitionState::L;
            } else if(lookAtCell_i == 0 && lookAtCell_j == 1) {
                currStateDiag = TransitionState::Y;
            } else if(lookAtCell_i > 1 && lookAtCell_j == 0) {
                currStateDiag = TransitionState::U;
            } else if(lookAtCell_i == 1 && lookAtCell_j == 0) {
                currStateDiag = TransitionState::X;
            } else if(lookAtCell_i == 0 && lookAtCell_j == 0) {
                currStateDiag = TransitionState::B;
            } else {
                currStateDiag = this->statesTable2D->at(lookAtCell_i - 1, lookAtCell_j - 1);
            }
            nextStateDiag = (currUpperSeqChar == currLowerSeqChar) ? TransitionState::MATCH : TransitionState::MISS;

            fromDiag = this->transitionProbabilitiesMatrix[static_cast<uint8_t>(currStateDiag)][static_cast<uint8_t>(nextStateDiag)]
                      + this->emissionProbabilitiesMatrix[NW::nucleotideBase2Index(currUpperSeqChar)][NW::nucleotideBase2Index(
                    currLowerSeqChar)]
                      + NWTable->at(lookAtCell_i, lookAtCell_j);

#ifdef END_STATE_HANDLING
            // End state handling
            if(i == nrow && j == ncol) {
                fromDiag += this->transitionProbabilitiesMatrix[static_cast<uint8_t>(nextStateDiag)][static_cast<uint8_t>(TransitionState::E)];
            }
#endif

            // Find the max value, and update the tables   #############################################################
            const auto& ret = max(
                    {pair{fromLeft, nextStateLeft}, pair{fromTop, nextStateTop}, pair{fromDiag, nextStateDiag}},
                                  [](const pair<double, TransitionState>& a, const pair<double, TransitionState>& b) -> bool {
                // This lambda returns true if a is less than b
                return a.first < b.first;
            });

            NWTable->at(i, j) = ret.first;
            this->statesTable2D->at(i-1, j-1) = ret.second;
        }
    }

    this->traceback(nrow, ncol);
    this->aligned = true;
    return AlignmentAlgorithm::alignSequences();
}

void NeedlemanWunschAlgorithm::fillEmissionsMatrix() {
    size_t n = this->trainingSequences.size();

    for(int i=0; i<n-1; i++) {
        for (int j = i + 1; j < n; j++) {
            const auto& firstSeq = this->trainingSequences[i];
            const auto& secondSeq = this->trainingSequences[j];

            auto firstSeqIt = firstSeq.cbegin();
            auto secondSeqIt = secondSeq.cbegin();

            // TODO: (Maybe?)Check whether both sequences are of the same size
            size_t seqN = firstSeq.size();
            for(int k=0; k < seqN; ++k, ++firstSeqIt, ++secondSeqIt) {
                char firstChar = *firstSeqIt;
                char secondChar = *secondSeqIt;

                uint8_t firstIndex = NW::nucleotideBase2Index(firstChar);
                uint8_t secondIndex = NW::nucleotideBase2Index(secondChar);

                // TODO: Symmetric matrix, maybe this can be simplified somehow?
                this->emissionsMatrix[firstIndex][secondIndex]++;
                this->emissionsMatrix[secondIndex][firstIndex]++;
            }
        }
    }
}

void NeedlemanWunschAlgorithm::fillTransitionsMatrix() {
    size_t n = this->trainingSequences.size();
    for(int i = 0; i<n-1; i++) {
        for (int j = i + 1; j < n; j++) {
            const auto& firstSeq = this->trainingSequences[i];
            const auto& secondSeq = this->trainingSequences[j];
            TransitionState currState = TransitionState::B, nextState;

            auto firstSeqIt = firstSeq.cbegin();
            auto secondSeqIt = secondSeq.cbegin();

            // TODO: Check whether both sequences are of the same size, maybe???
            size_t seqN = firstSeq.size();
            for(int k = 0; k < seqN; k++, ++firstSeqIt, ++secondSeqIt) {
                char firstChar = *firstSeqIt;
                char secondChar = *secondSeqIt;

                // Skip <'-';'-'> lines
                if(firstChar == '-' && secondChar == '-') {
                    continue;
                }

                if(firstChar == MyUtil::Algorithm::GAP && secondChar != MyUtil::Algorithm::GAP) {
                    // X or U diff here
                    if(currState == TransitionState::X || currState == TransitionState::U) {
                        nextState = TransitionState::U;
                    } else {
                        nextState = TransitionState::X;
                    }
                } else if(firstChar != MyUtil::Algorithm::GAP && secondChar == MyUtil::Algorithm::GAP) {
                    // Y or L diff here
                    if(currState == TransitionState::Y || currState == TransitionState::L) {
                        nextState = TransitionState::L;
                    } else {
                        nextState = TransitionState::Y;
                    }
                } else if(firstChar != MyUtil::Algorithm::GAP && secondChar != MyUtil::Algorithm::GAP) {
                    // Now check if they are equal
                    if(firstChar == secondChar) {
                        // Match
                        nextState = TransitionState::MATCH;
                    } else {
                        // Mismatch
                        nextState = TransitionState::MISS;
                    }
                }

                this->transitionsMatrix[static_cast<uint8_t>(currState)][static_cast<uint8_t>(nextState)]++;

//                // Handling end state
//                if(k == (seqN-1)) {
//                    this->transitionsMatrix[static_cast<uint8_t>(nextState)][static_cast<uint8_t>(TransitionState::E)]++;
//                }

                currState = nextState;
            }

            // Handling end state
            this->transitionsMatrix[static_cast<uint8_t>(currState)][static_cast<uint8_t>(TransitionState::E)]++;
        }
    }
}

void NeedlemanWunschAlgorithm::calculateEmissionProbabilities() {
    int totalSum = 0;

    // Calculating the total totalSum of probabilities
    for(int i = 0; i < MyUtil::Algorithm::NeedlemanWunsch::EMISSIONS_MATRIX_CONST-1; i++) {
        for(int j = 0; j < MyUtil::Algorithm::NeedlemanWunsch::EMISSIONS_MATRIX_CONST-1; j++) {
            totalSum += this->emissionsMatrix[i][j];
        }
    }

    // Calculating probabilities for the first 4 rows and columns(M)
    for(int i = 0; i < MyUtil::Algorithm::NeedlemanWunsch::EMISSIONS_MATRIX_CONST-1; i++) {
        for(int j = 0; j < MyUtil::Algorithm::NeedlemanWunsch::EMISSIONS_MATRIX_CONST-1; j++) {
            this->emissionProbabilitiesMatrix[i][j] = (double)this->emissionsMatrix[i][j]/totalSum;
        }
    }

    // Calculating probabilities for the 5. row and column(the GAPs)
    int totalSumRowGap = 0;
    int totalSumColGap = 0;

    for(int i = 0; i < NW::EMISSIONS_MATRIX_CONST - 1; i++) {
        totalSumColGap += this->emissionsMatrix[i][NW::EMISSIONS_MATRIX_CONST - 1];     // 5th Col
        totalSumRowGap += this->emissionsMatrix[NW::EMISSIONS_MATRIX_CONST - 1][i];     // 5th Row
    }


    for(int i = 0; i < NW::EMISSIONS_MATRIX_CONST - 1; i++) {
        this->emissionProbabilitiesMatrix[i][NW::EMISSIONS_MATRIX_CONST - 1] = (double)this->emissionsMatrix[i][NW::EMISSIONS_MATRIX_CONST - 1] / totalSumColGap;
        this->emissionProbabilitiesMatrix[NW::EMISSIONS_MATRIX_CONST - 1][i] = (double)this->emissionsMatrix[NW::EMISSIONS_MATRIX_CONST - 1][i] / totalSumRowGap;
    }
}

void NeedlemanWunschAlgorithm::calculateTransitionProbabilities() {
    for(int i = 0; i < NW::TRANSITIONS_MATRIX_CONST; i++) {
        // Skip the row corresponding to the End state
        if(i == static_cast<uint8_t>(TransitionState::E)) {
           continue;
        }

        int totalSum = 0;
        for(int j = 0; j < NW::TRANSITIONS_MATRIX_CONST; j++) {
            totalSum += this->transitionsMatrix[i][j];
        }

        for(int j = 0; j < NW::TRANSITIONS_MATRIX_CONST; j++) {
            this->transitionProbabilitiesMatrix[i][j] = (double)this->transitionsMatrix[i][j] / totalSum;
        }
    }

    // Set End row probabilities to 0.0
    for(int j = 0; j < NW::TRANSITIONS_MATRIX_CONST; j++) {
        this->transitionProbabilitiesMatrix[static_cast<uint8_t>(TransitionState::E)][j] = 0.0;
    }
}

void
NeedlemanWunschAlgorithm::traceback(size_t st_rows, size_t st_cols) {
/*
     * Go from the back of the sequences to the start and insert gaps('-') along the way. Do this while backtracing
     * in the states table.
     *
     * B => done
     * Y or L => go left in the table, insert gaps into lower sequence, move back in the other seq
     * X or U => go up in the table, insert gaps into upper sequence, move back in the other seq
     * MATCH or MISS => go diagonally in the table, do not insert gaps but move back into both sequences
     */

    cout << "Dynamic programming table filled. Tracebacking..." << endl;

    auto& upperSeq = this->toAlignSequences[NW::PAIR_UPPER_SEQ_INDEX];
    auto& lowerSeq = this->toAlignSequences[NW::PAIR_LOWER_SEQ_INDEX];
    auto upperSeqIt = upperSeq.end();
    auto lowerSeqIt = lowerSeq.end();
//    upperSeqIt--;           // end() points past the last element, so we need to move it back 1 place
//    lowerSeqIt--;           // end() points past the last element, so we need to move it back 1 place

    int64_t currRow = st_rows-1, currCol = st_cols-1;

    while(currRow >= 0 && currCol >= 0) {
        TransitionState currState = this->statesTable2D->at(currRow, currCol);
        switch(currState) {
            case TransitionState::Y:
            case TransitionState::L:
                currCol--;          // Go left in the table

                // Insert gaps into lower sequence, move back in the upper seq
                lowerSeq.insert(lowerSeqIt, MyUtil::Algorithm::GAP);

                break;
            case TransitionState::X:
            case TransitionState::U:
                currRow--;      // Go up in the table

                // Insert gaps into upper sequence, move back in the lower seq
                upperSeq.insert(upperSeqIt, MyUtil::Algorithm::GAP);

                break;
            case TransitionState::MATCH:
            case TransitionState::MISS:
                // Going diagonally
                currRow--;
                currCol--;

                // Move back in both sequences, don't insert any characters/gaps
                break;
        }

        --upperSeqIt;
        --lowerSeqIt;
    }

    // Manually handle here the 0th row(which is equal to currRow and/or currCol being -1);
    // Need to arrive at state B -> cell (-1, -1)
    if(currRow == -1 && currCol == -1) {
        // HMMState B -> Done?
    } else if(currRow == -1) {
        // Y or L -> Go left in the table, insert gaps into lower sequence, move back in the upper seq
        while(currCol != -1) {
            lowerSeq.emplace_front(MyUtil::Algorithm::GAP);
            --currCol;
        }
    } else {
        // X or U -> Go up, insert gaps into upper sequence, move back in the lower seq
        while(currRow != -1) {
            upperSeq.emplace_front(MyUtil::Algorithm::GAP);
            --currRow;
        }
    }

    cout << "Needleman-Wunsch alignment complete!" << endl;
}

std::string NeedlemanWunschAlgorithm::matricesString() const {
    stringstream ss;

    ss << "Emissions matrix:" << endl;
    ss << "Identifiers: A C G T -" << endl;
    ss << emissionsMatrixToString<uint64_t>(&this->emissionsMatrix[0][0]) << endl;

    ss << "Transitions matrix:" << endl;
    ss << "Identifiers: B, E, MATCH, MISS, X, Y, U, L" << endl;
    ss << transitionsMatrixToString<uint64_t>(&this->transitionsMatrix[0][0]) << endl;

    ss << "Emission probabilities matrix:" << endl;
    ss << emissionsMatrixToString<double>(&this->emissionProbabilitiesMatrix[0][0]) << endl;

    ss << "Transition probabilities matrix:" << endl;
    ss << transitionsMatrixToString<double>(&this->transitionProbabilitiesMatrix[0][0]) << endl;

    ss << "Transition States table:" << endl;
    ss << transitionStateMatrixToString();

    return ss.str();
}

std::string NeedlemanWunschAlgorithm::transitionStateMatrixToString() const {
    stringstream ss;

    const auto& upperSeq = this->toAlignSequences[NW::PAIR_UPPER_SEQ_INDEX];
    const auto& lowerSeq = this->toAlignSequences[NW::PAIR_LOWER_SEQ_INDEX];

    size_t cols = this->statesTable2D.getCols();
    size_t rows = this->statesTable2D.getRows();

    // Into first line put upper sequence
    ss << " |";

    for(const auto& element : upperSeq) {
        if(element != '-') {
            ss << fmt::format("{:^5}|", element);
        }
    }
    ss << endl;

    ss << "--";
    for(int i = 0; i < 6*cols-1; i++) {
        ss << '-';
    }
    ss << '|' << endl;

    // The actual table data
    // Make a copy of the lower sequence and remove all the '-'
    auto lowerSeqCopy = lowerSeq;
    lowerSeqCopy.remove('-');
    auto lowerSeqCopyIt = lowerSeqCopy.begin();
    for(int i = 0; i < rows; i++) {
        // Print the character from the sequence
        ss << *(lowerSeqCopyIt++) << '|';
        for(int j = 0; j < cols; j++) {
            TransitionState transState = this->statesTable2D->at(i, j);
            ss << fmt::format("{:^5s}|", NW::transitionIdentifiers[static_cast<uint8_t>(transState)]);
        }
        ss << endl;
    }

    // Table bed
    ss << "--";
    for(int i = 0; i < 6*cols-1; i++) {
        ss << '-';
    }
    ss << '|' << endl;

    return ss.str();
}