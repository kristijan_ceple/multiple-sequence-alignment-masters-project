#include <iostream>
#include "MyUtil.h"
#include "ProgressiveMSA_HMM_Alignment_Algorithm.h"
#include "NeedlemanWunschAlgorithm.h"
using namespace std;

void simple_test_driver_1() {
    vector<list<char>> seqs = MyUtil::IOOps::readSequencesFile("ToAlignSequences.multifasta");
    cout << "Read the sequences to align: " << endl;
    for(const auto& seq : seqs) {
        cout << string(seq.begin(), seq.end()) << endl;
    }

    vector<list<char>> startingMSA = MyUtil::IOOps::readSequencesFile("StartingMSA.multifasta");
    cout << "Read starting MSA: " << endl;
    for(const auto& seq : startingMSA) {
        cout << string(seq.begin(), seq.end()) << endl;
    }

    seqs.insert(end(seqs), begin(startingMSA), end(startingMSA));

    ProgressiveMSA_HMM_Alignment_Algorithm algo{seqs};
    const auto& res = algo.alignSequences();

    cout << "Result: " << endl;
    for(const auto& seq : res) {
        cout << string(seq.begin(), seq.end()) << endl;
    }
}

void NW_test_driver_1() {
    vector<list<char>> startingMSA = MyUtil::IOOps::readSequencesFile("HIV1_REF_2010_genome_DNA.fasta");
    cout << "Read starting MSA: " << endl;
    for(const auto& seq : startingMSA) {
        cout << string(seq.begin(), seq.end()) << endl;
    }
    cout << endl << endl;

    vector<list<char>> seqsToAlignVector = MyUtil::IOOps::readSequencesFile("toAlignPair6.fasta");
    cout << "Read the sequences to align: " << endl;
    for(const auto& seq : seqsToAlignVector) {
        cout << string(seq.begin(), seq.end()) << endl;
    }

    NeedlemanWunschAlgorithm NW(startingMSA);

    std::array<std::list<char>, 2> seqsToAlignArray;
    seqsToAlignArray[0] = move(seqsToAlignVector[0]);
    seqsToAlignArray[1] = move(seqsToAlignVector[1]);
    seqsToAlignVector.clear();

    NW.setSequencesToAlign(seqsToAlignArray);
    NW.alignSequences();

    std::cout << endl << endl << "Aligned sequences: " << endl;
    const auto& upperSeq = seqsToAlignArray[0];
    const auto& lowerSeq = seqsToAlignArray[1];
    cout << string(upperSeq.begin(), upperSeq.end()) << endl;
    cout << string(lowerSeq.begin(), lowerSeq.end()) << endl;

    cout << endl << "[INFO] Printing NW Matrices:" << endl;
    cout << NW.matricesString() << endl;

    return;
}

int main() {
    cout << "Welcome to Progressive MSA bioinformatics program!" << endl;
    NW_test_driver_1();

    return 0;
}
