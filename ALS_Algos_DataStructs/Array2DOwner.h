//
// Created by kikyy_99 on 31. 12. 2021..
//

#ifndef MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_ARRAY2DOWNER_H
#define MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_ARRAY2DOWNER_H

#include <memory>
#include <iostream>
template<typename T>
class Array2DAdapter;


template<typename T>
class Array2DOwner {
private:
    std::unique_ptr<T> ownedArray{};
    Array2DAdapter<T> ownedArray2D{};

    size_t rows = 0, cols = 0;
public:
    Array2DOwner() =default;
    Array2DOwner(T* arr, size_t rows_num, size_t cols_num) : rows{rows_num}, cols{cols_num} {
        this->ownedArray.reset(arr);
        this->ownedArray2D = Array2DAdapter<T>(arr, cols_num);
    }
    inline Array2DAdapter<T>& operator*();
    inline Array2DAdapter<T>* operator->();
    inline const Array2DAdapter<T>& operator*() const;
    inline const Array2DAdapter<T>* operator->() const;

    // Move assignment operator
    Array2DOwner<T>& operator=(Array2DOwner<T>&& other) noexcept;

    // Getters
    [[nodiscard]] size_t getRows() const;
    [[nodiscard]] size_t getCols() const;
};

template<typename T>
inline Array2DAdapter<T>& Array2DOwner<T>::operator*() {
    return ownedArray2D;
}

template<typename T>
inline Array2DAdapter<T>* Array2DOwner<T>::operator->() {
    return &ownedArray2D;
}

template<typename T>
inline const Array2DAdapter<T> &Array2DOwner<T>::operator*() const {
    return ownedArray2D;
}

template<typename T>
inline const Array2DAdapter<T> *Array2DOwner<T>::operator->() const {
    return &ownedArray2D;
}

template<typename T>
Array2DOwner<T>& Array2DOwner<T>::operator=(Array2DOwner<T> &&other)  noexcept {
    // Guard self assignment
    if(this == &other) {
        return *this;
    }

    this->ownedArray.release();
    this->ownedArray = std::move(other.ownedArray);
    this->ownedArray2D = std::move(other.ownedArray2D);
    this->cols = std::exchange(other.cols, 0);
    this->rows = std::exchange(other.rows, 0);
    return *this;
}

template<typename T>
size_t Array2DOwner<T>::getRows() const {
    return this->rows;
}

template<typename T>
size_t Array2DOwner<T>::getCols() const {
    return this->cols;
}


#endif //MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_ARRAY2DOWNER_H
