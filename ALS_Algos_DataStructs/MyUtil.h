//
// Created by kikyy_99 on 07.10.21.
//

#ifndef MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_MYUTIL_H
#define MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_MYUTIL_H


#include <string>
#include <vector>
#include <array>
#include <list>
#include <stdexcept>
#include <cmath>
#include <sstream>
#include <QString>
#include <fmt/format.h>
#include "Array2DAdapter.h"
#include "ProfileHMM.h"

namespace MyUtil {
    namespace GUI {
        using namespace std;
        const QString NW_STRING = "Needleman-Wunsch";
        const QString MSA_STRING = "MSA";
    }

    namespace IOOps {
        using namespace std;
        vector<list<char>> readSequencesFile(const std::string& filename);
        vector<string> readFile(const std::string& filename);
    }

    namespace Algorithm {
        using namespace std;
        inline constexpr char GAP = '-';

        inline void logOddsSquareMatrix(double* matrix, int dimension);
        template<typename T>
        string matrixToString(const T* matrix, int dimension);
//        template<typename T>
//        string matrixToString(const T* matrix, int dimension, const vector<string>& identifiers);
        inline string sequencesToString(const vector<std::list<char>>& sequences);
        inline string sequencesToString(const vector<std::list<char>>& sequences, ProfileHMM& profileHMM);

        namespace ProgressiveMSA_HMM_Alignment {
            inline constexpr double EMISSION_PSEUDOCOUNT_DEFAULT_VALUE = 0;
            inline constexpr double TRANSITION_PSEUDOCOUNT_DEFAULT_VALUE = 0;
            inline constexpr double COLUMN_REMOVAL_THRESHOLD_DEFAULT_VALUE = 0.21;
        }

        namespace NeedlemanWunsch {
            inline constexpr int EMISSIONS_MATRIX_CONST = 5;        // A, T, G, C, -, and O(Others);    TODO: Implement Others
            inline constexpr int TRANSITIONS_MATRIX_CONST = 8;      // B, E, MATCH. MISS, X, Y, U (repeated upper gap) and L (repeated lower gap)
            inline constexpr std::array<char, 5> emissionIdentifiers{'A', 'C', 'G', 'T', '-'};
            inline constexpr std::array<const char*, 8> transitionIdentifiers{"B", "E", "MATCH", "MISS", "X", "Y", "U", "L"};

            enum class NucleotideBase: uint8_t {A = 0, C = 1, G = 2, T = 3, GAP = 4};
            enum class TransitionState: uint8_t {B = 0, E = 1, MATCH = 2, MISS = 3, X = 4, Y = 5, U = 6, L = 7};
            enum class Direction: uint8_t {LEFT = 0, TOP = 1, DIAG = 2};

            inline constexpr int GAP_POINTS = -1;
            inline constexpr int MATCH_POINTS = -2;

            inline constexpr int PAIR_UPPER_SEQ_INDEX = 0;
            inline constexpr int PAIR_LOWER_SEQ_INDEX = 1;

            template<typename T>
            string emissionsMatrixToString(const T* matrix);
            template<typename T>
            string transitionsMatrixToString(const T* matrix);

            constexpr inline uint8_t nucleotideBase2Index(char in) {
                switch(in) {
                    case 'A':
                        return 0;
                    case 'C':
                        return 1;
                    case 'G':
                        return 2;
                    case 'T':
                        return 3;
//                    case '-':
//                        return 4;
                    default:
                        return 4;       // TODO: For now, '-' and all Other characters, but will have to check this later
//                        throw std::invalid_argument("Wrong input to function nucleotideBase2Index(char)!");
                }
            }
        }
    }
}

namespace NW = MyUtil::Algorithm::NeedlemanWunsch;
namespace MSA = MyUtil::Algorithm::ProgressiveMSA_HMM_Alignment;

inline void  MyUtil::Algorithm::logOddsSquareMatrix(double* matrix, int dimension) {
    Array2DAdapter matrixArr(matrix, dimension);

    for(int i = 0; i < dimension; i++) {
        for(int j = 0; j < dimension; j++) {
            double& num = matrixArr.at(i, j);
            num = log(num);
        }
    }
}

template<typename T>
std::string MyUtil::Algorithm::matrixToString(const T *matrix, int dimension) {
    stringstream ss;

    for(int i = 0; i < dimension; i++) {
        for(int j = 0; j < dimension; j++) {
            ss << matrix[i* dimension + j] << "  ";
        }
        ss << endl;
    }
    ss << endl;

    return ss.str();
}

std::string MyUtil::Algorithm::sequencesToString(const std::vector<std::list<char>> &sequences, ProfileHMM& profileHMM) {
    stringstream ss;
//    profileHMM.addLastConservedRegion();

    const auto& conservedRegions = profileHMM.getConservedRegions();
    int MSALen = sequences.front().size();
//    int conservRegSize = conservedRegions.size();
    int nextConservRegNum, nextConservRegIndex, MSAIndex;

    // Insert '*' and '/' here
    nextConservRegIndex = 0;
    MSAIndex = 0;
    while(MSAIndex < MSALen) {
        if(nextConservRegIndex >= conservedRegions.size()) {
            nextConservRegNum = MSALen;
        } else {
            nextConservRegNum = conservedRegions[nextConservRegIndex++];
        }

        while(MSAIndex < nextConservRegNum) {
            ss << '/';
            ++MSAIndex;
        }

        if(nextConservRegNum < MSALen) {
            ss << '*';
            ++MSAIndex;
        } else {
            break;
        }
    }

    // Sequences in between
    ss << endl << sequencesToString(sequences);

    // Insert '*' and '/' here
    nextConservRegIndex = 0;
    MSAIndex = 0;
    while(MSAIndex < MSALen) {
        if(nextConservRegIndex >= conservedRegions.size()) {
            nextConservRegNum = MSALen;
        } else {
            nextConservRegNum = conservedRegions[nextConservRegIndex++];
        }

        while(MSAIndex < nextConservRegNum) {
            ss << '/';
            ++MSAIndex;
        }

        if(nextConservRegNum < MSALen) {
            ss << '*';
            ++MSAIndex;
        } else {
            break;
        }
    }

//    profileHMM.removeLastConservedRegion();
    return ss.str();
}

std::string MyUtil::Algorithm::sequencesToString(const std::vector<std::list<char>> &sequences) {
    stringstream ss;

    for(auto const& seq : sequences) {
        ss << std::string(seq.begin(), seq.end()) << endl;
    }

    return ss.str();
}

template<typename T>
std::string NW::emissionsMatrixToString(const T *matrix) {
    stringstream ss;

    ss << " |     A     |     C     |     G     |     T     |     -     |" << endl;
    ss << "-------------------------------------------------------------|" << endl;
    for(int i = 0; i < NW::EMISSIONS_MATRIX_CONST; i++) {
        // Add starting identifier and |
        ss << NW::emissionIdentifiers[i] << '|';
        for(int j = 0; j < NW::EMISSIONS_MATRIX_CONST; j++) {
            ss << fmt::format("{:^11.4}|", static_cast<double>(matrix[i * NW::EMISSIONS_MATRIX_CONST + j]));
        }

        ss << endl;
        ss << "-------------------------------------------------------------|" << endl;
    }
    ss << endl;

    return ss.str();
}

template<typename T>
std::string NW::transitionsMatrixToString(const T *matrix) {
    stringstream ss;

    ss << "      |     B     |     E     |   MATCH   |    MISS   |     X     |     Y     |     U     |     L     |" << endl;
    ss << "------------------------------------------------------------------------------------------------------|" << endl;
    for(int i = 0; i < NW::TRANSITIONS_MATRIX_CONST; i++) {
        // Add starting identifier and |
        ss << fmt::format("{:^6}", NW::transitionIdentifiers[i]) << '|';
        for(int j = 0; j < NW::TRANSITIONS_MATRIX_CONST; j++) {
            ss << fmt::format("{:^11.4g}|", static_cast<double>(matrix[i * NW::TRANSITIONS_MATRIX_CONST + j]));
        }

        ss << endl;
        ss << "------------------------------------------------------------------------------------------------------|" << endl;
    }
    ss << endl;

    return ss.str();
}


#endif //MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_MYUTIL_H
