//
// Created by kikyy_99 on 10.10.21.
//

#include "ProgressiveMSA_HMM_Alignment_Algorithm.h"
#include "NeedlemanWunschAlgorithm.h"
#include "ProfileHMM.h"
#include "Viterbi.h"
#include <iostream>
#include <fmt/ranges.h>
using namespace std;

ProgressiveMSA_HMM_Alignment_Algorithm::ProgressiveMSA_HMM_Alignment_Algorithm(
        std::vector<std::list<char>>& trainingSequences, std::vector<std::list<char>>& toAlignSequences,
        double emissionPseudocount, double transitionPseudocount, double columnRemovalThreshold
        )
    : AlignmentAlgorithm(trainingSequences, toAlignSequences),
      emissionPseudocount{emissionPseudocount}, transitionPseudocount{transitionPseudocount}, columnRemovalThreshold{columnRemovalThreshold}
    {
    if(toAlignSequences.size() < 3) {
        throw std::invalid_argument("Progressive MSA sequences to be aligned need to contain at least 3 sequences!");
    }
}

std::vector<std::list<char>> &ProgressiveMSA_HMM_Alignment_Algorithm::alignSequences() {
//    // For now, pop the last 2 sequences
//    for(int i = 0; i < 2; i++) {
//        list<char> tmp = std::move(this->sequences.back());
//        this->sequences.pop_back();
//        this->MSA.emplace_back(move(tmp));
//    }

    /*
     * Need to do Needleman - Wunsch for the first 2 sequences in the seqsToAlign.
     */
    NeedlemanWunschAlgorithm NW(this->trainingSequences, this->toAlignSequences);
    NW.alignSequences();
    this->AssimilatedMSALength = 2;
    cout << "Dynamic programming table results:" << endl;
    cout << NW.transitionStateMatrixToString() << endl;

    /*
     * Starting MSA(of 2 elements) is now ready.
     * Now construct profile HMM for the starting MSA, and run Viterbi for the 3rd sequence.
     * Repeat for every other sequence until all have been assimilated
     *
     * In each iteration:
     * this->AssimilatedMSALength have been assimilated.
     * (this->AssimilatedMSALength + 1)th is currently being assimilated
     */
    for(this->AssimilatedMSALength = 2; this->AssimilatedMSALength < this->toAlignSequences.size(); this->AssimilatedMSALength++) {
        ProfileHMM profileHMM(this->toAlignSequences, this->AssimilatedMSALength);
        cout << MyUtil::Algorithm::sequencesToString(this->toAlignSequences, profileHMM) << endl;
//        cout << profileHMM << endl;

        std::list<char>& toAlignSeq = this->toAlignSequences[this->AssimilatedMSALength];
        cout << "Currently aligning:\n" << std::string(toAlignSeq.begin(), toAlignSeq.end()) << endl;

        Viterbi vit(profileHMM);
        auto ret = vit.getViterbiPath(toAlignSeq);
        cout << "Viterbi Path:\n";
        for(int i = 0; i < ret.size(); i++) {
            cout << ret[i];
        }
        cout << endl;

        this->assimilateSeqIntoMSA(ret, this->AssimilatedMSALength, profileHMM);

        cout << endl << endl;
    }

    ProfileHMM profileHMM(this->toAlignSequences, this->AssimilatedMSALength);
    cout << MyUtil::Algorithm::sequencesToString(this->toAlignSequences, profileHMM) << endl << endl;

    return AlignmentAlgorithm::alignSequences();
}

void ProgressiveMSA_HMM_Alignment_Algorithm::assimilateSeqIntoMSA(
        const vector<Viterbi::VGState> &viterbiPath,
        int seqToAssimilateIndex,
        ProfileHMM& profileHMM
        ) {
    list<char>& seqToAssimilate = this->toAlignSequences[seqToAssimilateIndex];
    list<char> newSeq{};

//    viterbiPath.pop_back();
    int letterIndex = 0;                                // Counts the current index in pattern and line
    auto seqIt = seqToAssimilate.cbegin();

    /*
    Moramo pratiti na kojem se trenutno indeksu nalazimo u msa kako bismo mogli provjeriti sive zone. Znaci,
    ovaj indeks povecavamo cijelo vrijeme. No, kada ga koristimo, koristimo njegovu verziju uvecanu za
    insertionOffset.

    De fakto stoga je msaIndex = patternIndex + insertionOffset
    */
    int msaIndex = 0;
    int insertionSpilloverOffset = 0;
    //int hmmIndex = 0;                                     // Counts the current index in the HMM
    int patternLen = viterbiPath.size() - 1;                // Disregard End state
    int msaLen = this->toAlignSequences[0].size();          // Sequences at the beginning should be of the same size since they have already been aligned

    int nextConservRegIndex = 0;
    int nextConservRegNum;
    bool inConservedRegion;
    char currChar;

    auto& conservedRegions = profileHMM.conservedRegions;

    vector<list<char>::iterator> MSAIters;
    MSAIters.reserve(seqToAssimilateIndex);
    for(int i = 0; i < seqToAssimilateIndex; i++) {
        MSAIters.emplace_back(this->toAlignSequences[i].begin());
    }

    // 'S' and 'E' ignored
    // patternIndex follows letters in the Viterbi path(= pattern )
    for(int patternIndex = 0; patternIndex < patternLen; patternIndex++) {
        msaIndex = newSeq.size();
        /*
        Prvo provjeriti je li trenutan indeks siva ili bijela zona? Ako je siva zona onda cemo preskociti do
        slijedece bijele. Tako da na pocetku petlje treba povecati indeks!
        */
        if(nextConservRegIndex == conservedRegions.size()) {
            /*
            Nema vise bijelih regija, tako da smo definitivno u sivoj jer jos uvijek nismo izasli van MSA!
             */
            inConservedRegion = false;
            nextConservRegNum = msaLen;     // Namjerno stavimo ovo da je poslije kraja
        } else {
            // Provjeriti jesmo li u bijeloj ili sivoj
            nextConservRegNum = conservedRegions[nextConservRegIndex] + insertionSpilloverOffset;
            inConservedRegion = (msaIndex == nextConservRegNum);
        }

        // Dohvatiti komandu
        Viterbi::VGState command = viterbiPath[patternIndex];

        // E sada, ovisno o regiji i trenutnoj komandi radimo drugacije akcije
        if(!inConservedRegion) {
            /*
            U ovoj regiji - siva zona - moramo prvo provjeriti je li komanda I. Ako komanda nije I, onda mozemo do
            kraja sive regije staviti crtice i dalje nastaviti sa svojim poslom. Na kraju ne treba continue - nego
            fallamo through na sljedeci dio petlje koji ce obraditi non-I komandu

            Ako komanda je I, onda stavljamo svoje znakove u sivu regiju. Do kraja sive regije popunjavamo crticama.
            Ukoliko je siva regija kraca od broja nasih insertiona, moramo gore umetnuti crtice i povecati insertionSpilloverOffset.
            Na kraju continue, kako bi se pokupila slijedeca komanda.
            */
            if(command == Viterbi::VGState::I) {
                /*
                Trebati ce pobrojati broj nasih Insertiona nasuprot broju sivih regija u MSA
                3 slucaja:
                    1) Siva regija duza od broja nasih insertiona
                    2) Jednako su dugi - super
                    3) Siva regija kraca od broja nasih insertiona
                 */
                int greyLen = (nextConservRegNum + insertionSpilloverOffset) - msaIndex;

                // Brojanje insertiona - look ahead
                int patternIndexTmp = patternIndex;
                Viterbi::VGState commandTmp = Viterbi::VGState::I;
                int insLen = 0;
                while(commandTmp == Viterbi::VGState::I) {
                    insLen++;
                    commandTmp = viterbiPath[++patternIndexTmp];
                }

                // Trebamo obraditi 3 slucaja
                int diff = greyLen - insLen;
                if(diff >= 0) {
                    /*
                    Tu obradujemo 2 slucaja:
                    Siva regija je duza - super -> znaci staviti nasa slova i sve crtice onda
                    Jednako duge - isto super - znaci stavimo samo nasa slova, nema potreba za crticama
                     */
                    // Prvo stavljamo znakove
                    for(int i = 0; i < insLen; i++) {
                        currChar = *(seqIt++);
                        letterIndex++;
                        newSeq.emplace_back(currChar);

                        patternIndex++;         // Povecavamo jer prolazimo I stanja pojedinih znakova koje dodajemo
                        std::for_each(
                                MSAIters.begin(),
                                MSAIters.end(),
                                [](list<char>::iterator& in) { in++; }
                        );
                    }

                    // Do kraja popuniti crticama
                    for(int i = 0; i < diff; i++) {
                        newSeq.emplace_back('-');
                    }

                    // Na kraju pomaknuti indeks, continuati kako bi se radila slijedeca komanda poslije svih Ins-eva
                } else {
                    /*
                    Siva regija je kraca - morati cemo iznad u MSA dodavati '-'-e.
                    Trebalo bi crtice dodati na kraj sive regije. Dakle, zelimo prvo dodati slova do kraj sive regije,
                    a onda istovremeno dodavati slova nase sekvence i extendirati one iznad.
                    Povecati insertionSpilloverOffset
                    */
                    // Idemo dodati nasa slova
                    for(int i = 0; i < greyLen; i++) {
                        newSeq.emplace_back(*(seqIt++));
                        letterIndex++;

                        patternIndex++;     // Povecavamo jer prolazimo I stanja pojedinih znakova koje dodajemo
                        std::for_each(
                                MSAIters.begin(),
                                MSAIters.end(),
                                [](list<char>::iterator& in) { in++; }
                        );
                    }

                    /*
                    Sad kad smo dodali slova koliko stane u sivu regiju, moramo dodati ostatak slova u nas string,
                    a crtice u sekvence iznad
                    */
                    diff = -diff;
                    for(int i = 0; i < diff; i++) {
                        newSeq.emplace_back(*(seqIt++));
                        letterIndex++;

                        for(int j = 0; j < seqToAssimilateIndex; j++) {
                            this->toAlignSequences[j].emplace(MSAIters[j], '-');
                        }

                        insertionSpilloverOffset++;     // Ovo povecavamo ovdje zato sto se MSA produzava
                        patternIndex++;                 // Povecavamo jer prolazimo I stanja pojedinih znakova koje dodajemo
//                        std::for_each(
//                                MSAIters.begin(),
//                                MSAIters.end(),
//                                [](list<char>::iterator& in) { in++; }          // TODO: This can be optimised not to be incremented every time, but instead only once with the offset
//                        );
                    }

                    // Sva slova i crtice dodane, mozemo continuati i ici dalje sa sljedecom komandom
                }

                // Tu ide continue, jer zelimo da petlja dohvati i obradi i sljedecu komandu
                // Treba pomaknuti indeks na slijedecu komandu
                patternIndex--;         // Counter automatic loop incremenation
                continue;
            } else {
                /*
                Do kraja sive regije staviti crtice, a onda nastaviti dalje normalno sa HMM-om.
                 */
                for(int subMSAIndex = msaIndex; subMSAIndex < nextConservRegNum; subMSAIndex++) {
                    newSeq.emplace_back('-');
                }
                // Tu ne ide continue - jer zelimo da se trenutna komanda obradi
                nextConservRegIndex++;          // Pomaknuti na sljedecu bijelu regiju
            }

        } else {
            /*
            Bijela zona
            Ako je bio Insertion, moramo dodati taj znak, a u ostale sekvence dodati '-' - i tako za sve insertione.
            Prvo izbrojati broj Insertiona u patternu, advanceati indeks za to
            Zatim dalje nastaviti sa komandama
             */
            if(command == Viterbi::VGState::I) {
                // Brojanje insertiona - look ahead
                int patternIndexTmp = patternIndex;
                Viterbi::VGState commandTmp = Viterbi::VGState::I;
                int insLen = 0;
                while(commandTmp == Viterbi::VGState::I) {
                    insLen++;
                    commandTmp = viterbiPath[++patternIndexTmp];
                }

                for(int i = 0; i < insLen; i++) {
                    currChar = *(seqIt++);
                    letterIndex++;
                    newSeq.emplace_back(currChar);

                    // Treba dodati u sve ostale sekvence '-' na ta podrucja
                    for(int j = 0; j < seqToAssimilateIndex; j++) {
                        this->toAlignSequences[j].emplace(MSAIters[j], '-');
                    }

                    insertionSpilloverOffset++;     // Ovo povecavamo ovdje zato sto se MSA produzava
                    patternIndex++;         // Povecavamo jer prolazimo I stanja pojedinih znakova koje dodajemo
//                    std::for_each(
//                            MSAIters.begin(),
//                            MSAIters.end(),
//                            [](list<char>::iterator& in) { in++; }
//                    );
                }

                // Smanjiti indeks i continue na sljedecu non-I komandu
                patternIndex--;
                continue;
            } else {
                /*
                Ako nije bio Insertion, onda samo idemo dalje na procesiranje M ili D
                U bijeloj smo zoni, i pritome nismo u I(znaci ne radimo novu sivu zonu). Dakle mozemo advanceati
                indeks bijelih zona - jer je slijedeca bijela zona
                 */
//               currConservRegIndex++;
                nextConservRegIndex++;
            }
        }

        // Sada slijedi onaj lagani dio - gdje djelujemo ovisno radi li se o D ili M
        switch (command) {
            case Viterbi::VGState::M:
                newSeq.emplace_back(*(seqIt++));
                letterIndex++;
                break;
            case Viterbi::VGState::D:
                newSeq.emplace_back('-');
                break;
            // TODO: comment part below out later in order for it to be faster!
            default:
                throw invalid_argument("Only M or D expected here");
        }

        // Every time patternIndex increases, so must these iterators
        std::for_each(
                MSAIters.begin(),
                MSAIters.end(),
                [](list<char>::iterator& in) { in++; }
        );
    }

    // Check if there is an unconserved region left at the end
    msaLen = this->toAlignSequences[0].size();          // Grab the updated size
    msaIndex = newSeq.size();
    while(msaIndex < msaLen) {
        newSeq.emplace_back('-');
        msaIndex++;
    }

    // Zamijeniti u MSA
//    std::swap(seqToAssimilate, newSeq);
    this->toAlignSequences[seqToAssimilateIndex] = std::move(newSeq);
}
