//
// Created by kikyy_99 on 08.10.21.
//

#ifndef MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_NEEDLEMANWUNSCHALGORITHM_H
#define MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_NEEDLEMANWUNSCHALGORITHM_H


#include "AlignmentAlgorithm.h"
#include "MyUtil.h"
#include "Array2DOwner.h"
#include <array>
#include <unordered_map>

/**
 * Needleman-Wunsch is a \bPairwise alignment algorithm
 */
class NeedlemanWunschAlgorithm : public AlignmentAlgorithm {
protected:
    uint64_t emissionsMatrix[NW::EMISSIONS_MATRIX_CONST][NW::EMISSIONS_MATRIX_CONST]{};                                                      // Note the default value-init {} constructor! This initialises all values to 0!
    uint64_t transitionsMatrix[NW::TRANSITIONS_MATRIX_CONST][NW::TRANSITIONS_MATRIX_CONST]{};                                                // Note the default value-init {} constructor! This initialises all values to 0!
    double emissionProbabilitiesMatrix[NW::EMISSIONS_MATRIX_CONST][NW::EMISSIONS_MATRIX_CONST];                                              // No need to default value-init this matrix
    double transitionProbabilitiesMatrix[NW::TRANSITIONS_MATRIX_CONST][NW::TRANSITIONS_MATRIX_CONST];                                        // No need to default value-init this matrix
    Array2DOwner<MyUtil::Algorithm::NeedlemanWunsch::TransitionState> statesTable2D{};

    std::unordered_map<std::string, int> transitionsMap;
    std::unordered_map<std::string, int> emissionsMap;

    void initParameters();
    void fillEmissionsMatrix();
    void fillTransitionsMatrix();
    void calculateEmissionProbabilities();
    void calculateTransitionProbabilities();
    void traceback(size_t st_rows, size_t st_cols);
public:
    NeedlemanWunschAlgorithm() =delete;
    explicit NeedlemanWunschAlgorithm(std::vector<std::list<char>>& trainingSequences, std::vector<std::list<char>>& toAlignSequences);

    /**
     * If the seqsToAlign field has got more than 2 sequences, only the first two are aligned.
     * The rest are ignored
     *
     * @return the seqsToAlign field
     */
    std::vector<std::list<char>>& alignSequences() override;

    std::string matricesString() const;
    std::string transitionStateMatrixToString() const;
};


#endif //MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_NEEDLEMANWUNSCHALGORITHM_H
