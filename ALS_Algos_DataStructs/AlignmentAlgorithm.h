//
// Created by kikyy_99 on 10.10.21.
//

#ifndef MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_ALGORITHM_H
#define MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_ALGORITHM_H

#include <vector>
#include <list>

/**
 * Interface - abstract class
 */
class AlignmentAlgorithm {
protected:
    std::vector<std::list<char>>& trainingSequences;
    std::vector<std::list<char>>& toAlignSequences;
    bool aligned = false;
public:
    AlignmentAlgorithm() =delete;
    explicit AlignmentAlgorithm(std::vector<std::list<char>>& trainingSequences, std::vector<std::list<char>>& toAlignSequences);
    virtual std::vector<std::list<char>>& alignSequences();
    inline std::vector<std::list<char>>* getAlignedSequences() { return(this->aligned ? &this->toAlignSequences : nullptr); }
    virtual ~AlignmentAlgorithm() =0;
};


#endif //MULTIPLE_SEQUENCE_ALIGNMENT_MASTERS_PROJECT_ALGORITHM_H
