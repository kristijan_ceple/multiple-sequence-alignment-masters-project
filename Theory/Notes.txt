Accordingly, biologists often ignore columns for which the fraction of space
symbols is greater than or equal to a COLUMN REMOVAL THRESHOLD q

White region(Conserved)
Grey region(Unconserved/Non-Conserved)

Insertion: letters in grey regions
Deletion: dashes in white regions

Basically, there are as many white regions as are M/Ds, and Is are located in-between each M/D column to model grey region letters
D        D
    I        
M        M
